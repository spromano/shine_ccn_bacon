package bacon.model;

import bacon.model.CommunityDetector;
import iweb2.ch3.collaborative.model.Dataset;
import iweb2.ch3.collaborative.model.Item;
import iweb2.ch3.collaborative.model.User;
import iweb2.ch3.collaborative.recommender.Recommender;
import iweb2.ch4.model.Cluster;
import java.util.List;
import java.util.Map;

public interface BaconAnalyzer {
    public void setDataset(Dataset var1);

    public void setCommunityDetector(CommunityDetector var1);

    public void setRecommender(Recommender var1);

    public List<Cluster> detectCommunities();

    public Map<Integer, User> detectTypicalUsers();

    public Map<Integer, User> getTypicalUsers();

    public List<Cluster> getCommunities();

    public Map<Integer, Double> getContentEvaluationVector(Item var1);

    public double getContentEvaluationForCommunity(int var1, Item var2);

    public void printContentEvaluationVector(Item var1);

    public void printCommunityDetectionResults();

    public void printTypicalUsers();

    public void printCommunityDetectionSummary();

    public double getContentUtility(Item var1);

    public void printContentUtility(Item var1);
}

