package bacon.model;

import bacon.clustering.ClusteringEvaluation;
import iweb2.ch4.model.Cluster;
import iweb2.ch4.model.DataPoint;
import java.io.PrintStream;
import java.util.List;
import java.util.Map;

public abstract class CommunityDetector {
    protected DataPoint[] datapoints;
    protected int num_users;
    protected double[][] dist_matrix;
    protected boolean clustersBuilt;
    protected List<Cluster> communities;

    public CommunityDetector(DataPoint[] datapoints, double[][] dist_matrix) {
        this.datapoints = datapoints;
        this.num_users = datapoints.length;
        this.dist_matrix = dist_matrix;
        this.clustersBuilt = false;
    }

    public void run() {
        if (!this.isDone()) {
            this.communities = this.cluster();
            this.clustersBuilt = true;
        } else {
            System.out.println("Clustering already performed. Please, reset clusters before a new run.");
        }
    }

    public void reset() {
        if (this.isDone()) {
            this.clustersBuilt = false;
        }
    }

    public boolean isDone() {
        return this.clustersBuilt;
    }

    public double[][] getDistanceMatrix() {
        return this.dist_matrix;
    }

    public DataPoint[] getDataPoints() {
        return this.datapoints;
    }

    public int getDistanceMatrixSize() {
        return this.dist_matrix[0].length;
    }

    public int getNumCommunities() {
        if (this.isDone()) {
            return this.communities.size();
        }
        System.out.println("Clustering not performed yet. No cluster num can be returned.");
        return -1;
    }

    public List<Cluster> getCommunities() {
        if (this.isDone()) {
            return this.communities;
        }
        System.out.println("Clustering not yet performed. No clusters can be returned.");
        return null;
    }

    public void printClusterProperties() {
        if (this.isDone()) {
            ClusteringEvaluation evaluator = new ClusteringEvaluation(this.communities, this.dist_matrix);
            evaluator.printClusterProperties();
        } else {
            System.out.println("Warning: clustering not performed yet.");
        }
    }

    protected abstract List<Cluster> cluster();

    public abstract Map<Integer, DataPoint> getTypicalDataPoints();

    public abstract void printResults();

    public abstract void printDetectionSummary();

    public abstract void printResultsOnLogFile();
}

