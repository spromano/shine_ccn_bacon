import bacon.clustering.DBSCANAlgorithm;
import bacon.data.ArtificialDataLoader;
import bacon.data.ArtificialDataLoader_Jaccard;
import bacon.similarity.BaconUserBasedSimilarity;
import bacon.utils.Chronometer;
import bacon.utils.Configurator;
import bacon.utils.Log;
import bacon.utils.MatrixHandler;
import iweb2.ch3.collaborative.data.MovieLensData;
import iweb2.ch3.collaborative.data.MovieLensDataset;
import iweb2.ch3.collaborative.model.Dataset;
import iweb2.ch3.collaborative.similarity.naive.SimilarityMatrix;
import iweb2.ch4.model.Cluster;
import iweb2.ch4.model.DataPoint;
import java.io.PrintStream;
import java.util.List;

public class Test {
    public static void movielens_build_and_save_bacon_matrix() {
        Chronometer timer = new Chronometer();
        System.out.println("\nLoading data...");
        timer.startChronometer();
        String movielensDataDir = "C:\\Users\\Roberta\\Workspace\\BACON_\\data\\MovieLens\\100k";
        MovieLensDataset dataset = MovieLensData.createDataset(movielensDataDir, 0);
        timer.stopChronometer();
        System.out.println("...Data loaded in " + timer.getElapsedTime() + " s.");
        System.out.println("\nBuilding distance matrix...");
        timer.startChronometer();
        SimilarityMatrix sim = ClusteringPointsetBuilder.buildBaconSimilarityMatrix(dataset);
        double[][] distance_matrix = ClusteringPointsetBuilder.buildDistanceMatrix(sim);
        timer.stopChronometer();
        System.out.println("Matrix built in " + timer.getElapsedTime() + " s");
        System.out.println("\nBuilding points...");
        timer.startChronometer();
        ClusteringPointsetBuilder.createDatapoints(dataset, sim);
        timer.stopChronometer();
        System.out.println("Points built in " + timer.getElapsedTime() + " s");
        System.out.println("\nSaving distance matrix...");
        timer.startChronometer();
        MatrixHandler.saveMatrixOnFile(distance_matrix, "movielens_bacon_distance_matrix.txt");
        timer.stopChronometer();
        System.out.println("Matrix saved in " + timer.getElapsedTime() + " s");
        System.out.println("\nReading distance matrix...");
        timer.startChronometer();
        MatrixHandler.readMatrixFromFile("movielens_bacon_distance_matrix.txt");
        timer.stopChronometer();
        System.out.println("Matrix read in " + timer.getElapsedTime() + " s");
        System.out.println("Test complete!");
    }

    public static void movielens_bacon_tuning() {
        Chronometer timer = new Chronometer();
        System.out.println("\nLoading data...");
        timer.startChronometer();
        String movielensDataDir = "C:\\Users\\Roberta\\Workspace\\BACON_\\data\\MovieLens\\100k";
        MovieLensDataset dataset = MovieLensData.createDataset(movielensDataDir, 0);
        timer.stopChronometer();
        System.out.println("...Data loaded in " + timer.getElapsedTime() + " s.");
        System.out.println("\nBuilding points...");
        timer.startChronometer();
        DataPoint[] points = ClusteringPointsetBuilder.createDatapoints(dataset);
        timer.stopChronometer();
        System.out.println("Points built in " + timer.getElapsedTime() + " s");
        System.out.println("\nReading distance matrix...");
        timer.startChronometer();
        double[][] distance_matrix = MatrixHandler.readMatrixFromFile("movielens_bacon_distance_matrix.txt");
        timer.stopChronometer();
        System.out.println("Matrix read in " + timer.getElapsedTime() + " s");
        double STARTING_EPSILON = 0.75;
        double EPSILON_STEP = 0.01;
        double MAX_EPSILON = 0.95;
        int STARTING_MINPOINTS = 2;
        int MINPOINTS_STEP = 1;
        int MAX_MINPOINTS = 7;
        for (double epsilon = STARTING_EPSILON; epsilon < MAX_EPSILON; epsilon += EPSILON_STEP) {
            for (int minPoints = STARTING_MINPOINTS; minPoints < MAX_MINPOINTS; minPoints += MINPOINTS_STEP) {
                System.out.println("\nRunning DBSCAN...");
                timer.startChronometer();
                DBSCANAlgorithm dbscan = new DBSCANAlgorithm(points, distance_matrix, epsilon, minPoints);
                dbscan.cluster();
                timer.stopChronometer();
                System.out.println("DBSCAN completed in " + timer.getElapsedTime() + " s");
            }
        }
    }

    public static void artificial_bacon_tuning() {
        Chronometer timer = new Chronometer();
        System.out.println("\nLoading data...");
        timer.startChronometer();
        ArtificialDataLoader_Jaccard loader = new ArtificialDataLoader_Jaccard(1, 3);
        Dataset dataset = loader.loadDataset();
        timer.stopChronometer();
        System.out.println("...Data loaded in " + timer.getElapsedTime() + " s.");
        System.out.println("\nBuilding distance matrix...");
        timer.startChronometer();
        SimilarityMatrix sim = ClusteringPointsetBuilder.buildBaconSimilarityMatrix(dataset);
        double[][] distance_matrix = ClusteringPointsetBuilder.buildDistanceMatrix(sim);
        timer.stopChronometer();
        System.out.println("Matrix built in " + timer.getElapsedTime() + " s");
        System.out.println("\nBuilding points...");
        timer.startChronometer();
        DataPoint[] points = ClusteringPointsetBuilder.createDatapoints(dataset, sim);
        timer.stopChronometer();
        System.out.println("Points built in " + timer.getElapsedTime() + " s");
        double STARTING_EPSILON = 0.5;
        double EPSILON_STEP = 0.1;
        double MAX_EPSILON = 0.95;
        int STARTING_MINPOINTS = 2;
        int MINPOINTS_STEP = 1;
        int MAX_MINPOINTS = 7;
        for (double epsilon = STARTING_EPSILON; epsilon < MAX_EPSILON; epsilon += EPSILON_STEP) {
            for (int minPoints = STARTING_MINPOINTS; minPoints < MAX_MINPOINTS; minPoints += MINPOINTS_STEP) {
                System.out.println("\nRunning DBSCAN...");
                timer.startChronometer();
                DBSCANAlgorithm dbscan = new DBSCANAlgorithm(points, distance_matrix, epsilon, minPoints);
                dbscan.cluster();
                timer.stopChronometer();
                System.out.println("DBSCAN completed in " + timer.getElapsedTime() + " s");
            }
        }
    }

    public static void second_artificial_bacon_tuning() {
        Chronometer timer = new Chronometer();
        System.out.println("\nLoading data...");
        timer.startChronometer();
        ArtificialDataLoader loader = new ArtificialDataLoader(1, 3, 3);
        Dataset dataset = loader.loadDataset();
        timer.stopChronometer();
        System.out.println("...Data loaded in " + timer.getElapsedTime() + " s.");
        System.out.println("\nBuilding distance matrix...");
        timer.startChronometer();
        SimilarityMatrix sim = ClusteringPointsetBuilder.buildBaconSimilarityMatrix(dataset);
        double[][] distance_matrix = ClusteringPointsetBuilder.buildDistanceMatrix(sim);
        timer.stopChronometer();
        System.out.println("Matrix built in " + timer.getElapsedTime() + " s");
        System.out.println("\nBuilding points...");
        timer.startChronometer();
        DataPoint[] points = ClusteringPointsetBuilder.createDatapoints(dataset, sim);
        timer.stopChronometer();
        System.out.println("Points built in " + timer.getElapsedTime() + " s");
        double STARTING_EPSILON = 0.5;
        double EPSILON_STEP = 0.1;
        double MAX_EPSILON = 0.95;
        int STARTING_MINPOINTS = 2;
        int MINPOINTS_STEP = 1;
        int MAX_MINPOINTS = 7;
        for (double epsilon = STARTING_EPSILON; epsilon < MAX_EPSILON; epsilon += EPSILON_STEP) {
            for (int minPoints = STARTING_MINPOINTS; minPoints < MAX_MINPOINTS; minPoints += MINPOINTS_STEP) {
                System.out.println("\nRunning DBSCAN...");
                timer.startChronometer();
                DBSCANAlgorithm dbscan = new DBSCANAlgorithm(points, distance_matrix, epsilon, minPoints);
                dbscan.cluster();
                timer.stopChronometer();
                System.out.println("DBSCAN completed in " + timer.getElapsedTime() + " s");
            }
        }
    }

    public static void movielens_bacon_dbscan() {
        Chronometer timer = new Chronometer();
        System.out.println("\nLoading data...");
        timer.startChronometer();
        String movielensDataDir = "C:\\Users\\Roberta\\Workspace\\BACON_\\data\\MovieLens\\100k";
        MovieLensDataset dataset = MovieLensData.createDataset(movielensDataDir, 0);
        timer.stopChronometer();
        System.out.println("...Data loaded in " + timer.getElapsedTime() + " s.");
        System.out.println("\nBuilding distance matrix...");
        timer.startChronometer();
        BaconUserBasedSimilarity sim = new BaconUserBasedSimilarity(dataset);
        double[][] distance_matrix = ClusteringPointsetBuilder.buildDistanceMatrix(sim);
        timer.stopChronometer();
        System.out.println("Matrix built in " + timer.getElapsedTime() + " s");
        System.out.println("\nBuilding points...");
        timer.startChronometer();
        DataPoint[] points = ClusteringPointsetBuilder.createDatapoints(dataset, sim);
        timer.stopChronometer();
        System.out.println("Points built in " + timer.getElapsedTime() + " s");
        System.out.println("\nRunning DBSCAN...");
        timer.startChronometer();
        double epsilon = 0.8;
        int minPoints = 6;
        DBSCANAlgorithm dbscan = new DBSCANAlgorithm(points, distance_matrix, epsilon, minPoints);
        dbscan.cluster();
        timer.stopChronometer();
        System.out.println("DBSCAN completed in " + timer.getElapsedTime() + " s");
    }

    public static void artificial_bacon_dbscan() {
        Chronometer timer = new Chronometer();
        System.out.println("\nLoading data...");
        timer.startChronometer();
        ArtificialDataLoader_Jaccard loader = new ArtificialDataLoader_Jaccard(1, 3);
        Dataset dataset = loader.loadDataset();
        timer.stopChronometer();
        System.out.println("...Data loaded in " + timer.getElapsedTime() + " s.");
        System.out.println("\nBuilding distance matrix...");
        timer.startChronometer();
        SimilarityMatrix sim = ClusteringPointsetBuilder.buildBaconSimilarityMatrix(dataset);
        double[][] distance_matrix = ClusteringPointsetBuilder.buildDistanceMatrix(sim);
        timer.stopChronometer();
        System.out.println("Matrix built in " + timer.getElapsedTime() + " s");
        System.out.println("\nBuilding points...");
        timer.startChronometer();
        DataPoint[] points = ClusteringPointsetBuilder.createDatapoints(dataset, sim);
        timer.stopChronometer();
        System.out.println("Points built in " + timer.getElapsedTime() + " s");
        System.out.println("\nRunning DBSCAN...");
        timer.startChronometer();
        double epsilon = 0.8;
        int minPoints = 6;
        DBSCANAlgorithm dbscan = new DBSCANAlgorithm(points, distance_matrix, epsilon, minPoints);
        dbscan.cluster();
        timer.stopChronometer();
        System.out.println("DBSCAN completed in " + timer.getElapsedTime() + " s");
    }

    public static void movielens_jaccard_tuning() {
        Chronometer timer = new Chronometer();
        System.out.println("\nLoading data...");
        timer.startChronometer();
        String movielensDataDir = "C:\\Users\\Roberta\\Workspace\\BACON_\\data\\MovieLens\\100k";
        MovieLensDataset dataset = MovieLensData.createDataset(movielensDataDir, 0);
        timer.stopChronometer();
        System.out.println("...Data loaded in " + timer.getElapsedTime() + " s.");
        System.out.println("\nBuilding points...");
        timer.startChronometer();
        DataPoint[] points = ClusteringPointsetBuilder.createJaccardDatapoints(dataset);
        timer.stopChronometer();
        System.out.println("Points built in " + timer.getElapsedTime() + " s");
        System.out.println("\nReading distance matrix...");
        timer.startChronometer();
        double[][] jaccard_distance_matrix = MatrixHandler.readMatrixFromFile("movielens_jaccard_distance_matrix.txt");
        timer.stopChronometer();
        System.out.println("Matrix read in " + timer.getElapsedTime() + " s");
        double STARTING_EPSILON = 0.5;
        double EPSILON_STEP = 0.1;
        double MAX_EPSILON = 0.9;
        int STARTING_MINPOINTS = 6;
        int MINPOINTS_STEP = 1;
        int MAX_MINPOINTS = 7;
        for (double epsilon = STARTING_EPSILON; epsilon < MAX_EPSILON; epsilon += EPSILON_STEP) {
            for (int minPoints = STARTING_MINPOINTS; minPoints < MAX_MINPOINTS; minPoints += MINPOINTS_STEP) {
                System.out.println("\nRunning DBSCAN...");
                timer.startChronometer();
                DBSCANAlgorithm dbscan = new DBSCANAlgorithm(points, jaccard_distance_matrix, epsilon, minPoints);
                dbscan.cluster();
                timer.stopChronometer();
                System.out.println("DBSCAN completed in " + timer.getElapsedTime() + " s");
            }
        }
    }

    public static void movielens_jaccard_dbscan() {
        Chronometer timer = new Chronometer();
        System.out.println("\nLoading data...");
        timer.startChronometer();
        String movielensDataDir = "C:\\Users\\Roberta\\Workspace\\BACON_\\data\\MovieLens\\100k";
        MovieLensDataset dataset = MovieLensData.createDataset(movielensDataDir, 0);
        timer.stopChronometer();
        System.out.println("...Data loaded in " + timer.getElapsedTime() + " s.");
        System.out.println("\nBuilding points...");
        timer.startChronometer();
        DataPoint[] points = ClusteringPointsetBuilder.createJaccardDatapoints(dataset);
        timer.stopChronometer();
        System.out.println("Points built in " + timer.getElapsedTime() + " s");
        System.out.println("\nReading distance matrix...");
        timer.startChronometer();
        double[][] jaccard_distance_matrix = MatrixHandler.readMatrixFromFile("movielens_jaccard_distance_matrix.txt");
        timer.stopChronometer();
        System.out.println("Matrix read in " + timer.getElapsedTime() + " s");
        System.out.println("\nRunning DBSCAN...");
        timer.startChronometer();
        double epsilon = 0.8;
        int minPoints = 6;
        DBSCANAlgorithm dbscan = new DBSCANAlgorithm(points, jaccard_distance_matrix, epsilon, minPoints);
        dbscan.cluster();
        timer.stopChronometer();
        System.out.println("DBSCAN completed in " + timer.getElapsedTime() + " s");
    }

    public static void movielens_build_and_save_jaccard_matrix() {
        Chronometer timer = new Chronometer();
        System.out.println("\nLoading data...");
        timer.startChronometer();
        String movielensDataDir = "C:\\Users\\Roberta\\Workspace\\BACON_\\data\\MovieLens\\100k";
        MovieLensDataset dataset = MovieLensData.createDataset(movielensDataDir, 0);
        timer.stopChronometer();
        System.out.println("...Data loaded in " + timer.getElapsedTime() + " s.");
        System.out.println("\nBuilding points...");
        timer.startChronometer();
        DataPoint[] points = ClusteringPointsetBuilder.createJaccardDatapoints(dataset);
        timer.stopChronometer();
        System.out.println("Points built in " + timer.getElapsedTime() + " s");
        System.out.println("\nBuilding distance matrix...");
        timer.startChronometer();
        double[][] jaccard_distance_matrix = ClusteringPointsetBuilder.buildJaccardDistanceMatrix(points);
        timer.stopChronometer();
        System.out.println("Matrix built in " + timer.getElapsedTime() + " s");
        System.out.println("\nSaving distance matrix...");
        timer.startChronometer();
        MatrixHandler.saveMatrixOnFile(jaccard_distance_matrix, "movielens_jaccard_distance_matrix.txt");
        timer.stopChronometer();
        System.out.println("Matrix saved in " + timer.getElapsedTime() + " s");
        System.out.println("\nReading distance matrix...");
        timer.startChronometer();
        double[][] read_dist_mat = MatrixHandler.readMatrixFromFile("movielens_jaccard_distance_matrix.txt");
        timer.stopChronometer();
        System.out.println("Matrix read in " + timer.getElapsedTime() + " s");
        int matrix_dim = dataset.getUserCount();
        for (int i = 0; i < matrix_dim; ++i) {
            for (int j = 0; j < matrix_dim; ++j) {
                if (jaccard_distance_matrix[i][j] == read_dist_mat[i][j]) continue;
                throw new RuntimeException("Something is wrong");
            }
        }
        System.out.println("Test complete!");
    }

    public static void main(String[] args) {
        Test.artificial_2_graph();
    }

    public static void graph(Dataset dataset) {
        Chronometer timer = new Chronometer();
        System.out.println("\nBuilding distance matrix...");
        timer.startChronometer();
        SimilarityMatrix sim = ClusteringPointsetBuilder.buildBaconSimilarityMatrix(dataset);
        DataPoint[] points = ClusteringPointsetBuilder.createDatapoints(dataset, sim);
        double[][] distance_matrix = ClusteringPointsetBuilder.buildDistanceMatrix(sim);
        timer.stopChronometer();
        System.out.println("Matrix built in " + timer.getElapsedTime() + " s");
        Configurator conf = new Configurator();
        Log log = Log.getInstance();
        log.println("minpoints, epsilon, score, noise, details");
        double STARTING_EPSILON = conf.getMinEpsilon();
        double EPSILON_STEP = conf.getEpsilonStep();
        double MAX_EPSILON = conf.getMaxEpsilon();
        int STARTING_MINPOINTS = conf.getMinMinpoints();
        int MINPOINTS_STEP = conf.getMinpointsStep();
        int MAX_MINPOINTS = conf.getMaxMinpoints();
        for (double epsilon = STARTING_EPSILON; epsilon < MAX_EPSILON; epsilon += EPSILON_STEP) {
            for (int minPoints = STARTING_MINPOINTS; minPoints < MAX_MINPOINTS; minPoints += MINPOINTS_STEP) {
                System.out.println("\nRunning DBSCAN...");
                timer.startChronometer();
                DBSCANAlgorithm dbscan = new DBSCANAlgorithm(points, distance_matrix, epsilon, minPoints);
                List<Cluster> clusters = dbscan.cluster();
                timer.stopChronometer();
                System.out.println("DBSCAN completed in " + timer.getElapsedTime() + " s");
                dbscan.printSummary(clusters);
                int score = 0;
                String details = "";
                boolean there_is_noise = false;
                int noise = 0;
                for (Cluster c : clusters) {
                    if (Integer.parseInt(c.getLabel()) != DBSCANAlgorithm.CLUSTER_ID_NOISE) continue;
                    there_is_noise = true;
                    noise = c.size();
                }
                if (noise < dataset.getUserCount() / 2 && clusters.size() != 1) {
                    score = 1;
                }
                int[] populations = there_is_noise ? new int[clusters.size() - 1] : new int[clusters.size()];
                int index = 0;
                for (Cluster c : clusters) {
                    if (c.getLabel().equals(String.valueOf(DBSCANAlgorithm.CLUSTER_ID_NOISE))) continue;
                    populations[index] = c.size();
                    ++index;
                }
                if (populations != null) {
                    details = "Cluster population: ";
                    for (int pop : populations) {
                        details = String.valueOf(details) + pop + " ";
                    }
                }
                log.println(String.valueOf(minPoints) + ", " + epsilon + ", " + score + ", " + noise + ", " + details);
            }
        }
    }

    public static void artificial_1_2group_graph() {
        int num_groups = 2;
        int min_usf = 10;
        int max_usf = 1000;
        int usf_step = 100;
        int min_isf = 10;
        int max_isf = 1000;
        int isf_step = 10;
        double max_vp = 1.0;
        double min_vp = 0.1;
        double vp_step = 0.2;
        for (double votes_percentage = min_vp; votes_percentage < max_vp; votes_percentage += vp_step) {
            for (int users_scale_factor = min_usf; users_scale_factor < max_usf; users_scale_factor *= usf_step) {
                for (int items_scale_factor = min_isf; items_scale_factor < max_isf; items_scale_factor *= isf_step) {
                    ArtificialDataLoader loader = new ArtificialDataLoader(users_scale_factor, items_scale_factor, num_groups, votes_percentage);
                    Dataset dataset = loader.loadDataset();
                    Log log = Log.getInstance();
                    log.println("artificial_1, 2 groups, " + dataset.getUserCount() + " users, " + dataset.getItemCount() + " items, " + votes_percentage + " % of voted items ");
                    Test.graph(dataset);
                }
            }
        }
    }

    public static void artificial_1_3group_graph() {
        int num_groups = 3;
        int min_usf = 10;
        int max_usf = 1000;
        int usf_step = 100;
        int min_isf = 10;
        int max_isf = 1000;
        int isf_step = 10;
        double max_vp = 1.0;
        double min_vp = 0.1;
        double vp_step = 0.3;
        for (double votes_percentage = min_vp; votes_percentage < max_vp; votes_percentage += vp_step) {
            for (int users_scale_factor = min_usf; users_scale_factor < max_usf; users_scale_factor *= usf_step) {
                for (int items_scale_factor = min_isf; items_scale_factor < max_isf; items_scale_factor *= isf_step) {
                    ArtificialDataLoader loader = new ArtificialDataLoader(users_scale_factor, items_scale_factor, num_groups, votes_percentage);
                    Dataset dataset = loader.loadDataset();
                    Log log = Log.getInstance();
                    log.println("artificial_1, 3 groups, " + dataset.getUserCount() + " users, " + dataset.getItemCount() + " items, " + votes_percentage + " % of voted items ");
                    Test.graph(dataset);
                }
            }
        }
    }

    public static void artificial_2_graph() {
        int min_usf = 10;
        int max_usf = 1000;
        int usf_step = 10;
        int min_isf = 10;
        int max_isf = 1000;
        int isf_step = 10;
        double max_vp = 1.0;
        double min_vp = 0.1;
        double vp_step = 0.1;
        for (double votes_percentage = min_vp; votes_percentage < max_vp; votes_percentage += vp_step) {
            for (int users_scale_factor = min_usf; users_scale_factor < max_usf; users_scale_factor *= usf_step) {
                for (int items_scale_factor = min_isf; items_scale_factor < max_isf; items_scale_factor *= isf_step) {
                    ArtificialDataLoader_Jaccard loader = new ArtificialDataLoader_Jaccard(users_scale_factor, items_scale_factor, votes_percentage);
                    Dataset dataset = loader.loadDataset();
                    Log log = Log.getInstance();
                    log.println("artificial_2, " + dataset.getUserCount() + " users, " + dataset.getItemCount() + " items, " + votes_percentage + " % of random voted items ");
                    Test.graph(dataset);
                }
            }
        }
    }
}

