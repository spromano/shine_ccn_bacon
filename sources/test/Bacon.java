import bacon.analyzer.Validator;
import bacon.clustering.ClusteringPointsetBuilder;
import bacon.clustering.DBSCANAlgorithm;
import bacon.data.ArtificialDataLoader;
import bacon.data.ArtificialDataLoader_Jaccard;
import bacon.model.BaconAnalyzer;
import bacon.model.CommunityDetector;
import bacon.utils.Chronometer;
import bacon.utils.Configurator;
import bacon.utils.Log;
import iweb2.ch3.collaborative.data.MovieLensData;
import iweb2.ch3.collaborative.data.MusicData;
import iweb2.ch3.collaborative.model.Dataset;
import iweb2.ch3.collaborative.model.Item;
import iweb2.ch3.collaborative.model.User;
import iweb2.ch3.collaborative.similarity.naive.SimilarityMatrix;
import iweb2.ch4.model.Cluster;
import iweb2.ch4.model.DataPoint;
import java.io.PrintStream;
import java.util.List;
import java.util.Map;

public class Bacon {
    public static void main(String[] args) {
        Configurator conf = new Configurator();
        String test = conf.getTestMode();
        if (test.equals("basic")) {
            Dataset dataset = Bacon.buildDataset();
            BaconAnalyzerImpl analyzer = new BaconAnalyzerImpl(dataset);
            Bacon.performAnalysis(analyzer, dataset);
        } else if (test.equals("tuning")) {
            Bacon.performTuning();
        } else if (test.equals("validation")) {
            Dataset dataset = Bacon.buildDataset();
            BaconAnalyzerImpl analyzer = new BaconAnalyzerImpl(dataset);
            Bacon.performValidation(analyzer);
        } else if (test.equals("cluster_properties")) {
            Dataset dataset = Bacon.buildDataset();
            BaconAnalyzerImpl analyzer = new BaconAnalyzerImpl(dataset);
            Bacon.performClusteringEvaluation(analyzer);
        } else if (test.equals("graph")) {
            Bacon.graph();
        }
    }

    public static Dataset buildDataset() {
        Dataset dataset = null;
        Configurator conf = new Configurator();
        String data_mode = conf.getDataMode();
        if (data_mode.equals("music")) {
            double votes_percentage = conf.getMusicVotesPercentage();
            dataset = MusicData.createDataset(votes_percentage);
        } else if (data_mode.equals("artificial_1")) {
            int users_scale_factor = conf.getUserScaleFactor();
            int items_scale_factor = conf.getItemScaleFactor();
            int num_groups = conf.getNumGroup();
            double votes_percentage = conf.getVotesPercentage();
            ArtificialDataLoader loader = new ArtificialDataLoader(users_scale_factor, items_scale_factor, num_groups, votes_percentage);
            dataset = loader.loadDataset();
        } else if (data_mode.equals("artificial_2")) {
            int users_scale_factor_2 = conf.getUserScaleFactor_2();
            int items_scale_factor_2 = conf.getItemScaleFactor_2();
            double random_votes_percentage = conf.getRandomVotesPercentage();
            ArtificialDataLoader_Jaccard loader = new ArtificialDataLoader_Jaccard(users_scale_factor_2, items_scale_factor_2, random_votes_percentage);
            dataset = loader.loadDataset();
        } else if (data_mode.equals("movielens")) {
            String movielensDataDir = conf.getMovielensDataDir();
            dataset = MovieLensData.createDataset(movielensDataDir, 0);
        } else if (data_mode.equals("database")) {
            System.out.println("Database mode not implemented.");
        }
        return dataset;
    }

    public static void performAnalysis(BaconAnalyzer analyzer, Dataset dataset) {
        analyzer.detectCommunities();
        analyzer.detectTypicalUsers();
        analyzer.printCommunityDetectionSummary();
        if (dataset.getUserCount() < 500) {
            analyzer.printCommunityDetectionResults();
        }
        analyzer.printTypicalUsers();
        Item item = dataset.getItem(1);
        analyzer.printContentEvaluationVector(item);
        analyzer.printContentUtility(item);
    }

    public static void performValidation(BaconAnalyzerImpl analyzer) {
        analyzer.detectCommunities();
        analyzer.detectTypicalUsers();
        analyzer.printCommunityDetectionSummary();
        Chronometer timer = new Chronometer();
        System.out.println("\nStarting validation...");
        timer.startChronometer();
        Validator validator = new Validator(analyzer);
        if (validator.isThereNoise()) {
            validator.printNoisePercentage();
            validator.printPercentualAccuracyWithoutNoise();
        } else {
            validator.printPercentualAccuracy();
        }
        timer.stopChronometer();
        System.out.println("\nValidation completed in " + timer.getElapsedTime() + " s.");
    }

    public static void performClusteringEvaluation(BaconAnalyzerImpl analyzer) {
        analyzer.detectCommunities();
        analyzer.detectTypicalUsers();
        analyzer.printCommunityDetectionSummary();
        Chronometer timer = new Chronometer();
        System.out.println("\nStarting clustering evaluation...");
        timer.startChronometer();
        analyzer.getCommunityDetector().printClusterProperties();
        timer.stopChronometer();
        System.out.println("\nClustering evaluation completed in " + timer.getElapsedTime() + " s.");
    }

    public static void performTuning() {
        Dataset dataset = Bacon.buildDataset();
        DataPoint[] points = ClusteringPointsetBuilder.createDatapoints(dataset);
        Chronometer timer = new Chronometer();
        System.out.println("\nBuilding distance matrix...");
        timer.startChronometer();
        SimilarityMatrix sim = ClusteringPointsetBuilder.buildBaconSimilarityMatrix(dataset);
        double[][] distance_matrix = ClusteringPointsetBuilder.buildDistanceMatrix(sim);
        timer.stopChronometer();
        System.out.println("Matrix built in " + timer.getElapsedTime() + " s");
        Configurator conf = new Configurator();
        double STARTING_EPSILON = conf.getMinEpsilon();
        double EPSILON_STEP = conf.getEpsilonStep();
        double MAX_EPSILON = conf.getMaxEpsilon();
        int STARTING_MINPOINTS = conf.getMinMinpoints();
        int MINPOINTS_STEP = conf.getMinpointsStep();
        int MAX_MINPOINTS = conf.getMaxMinpoints();
        for (double epsilon = STARTING_EPSILON; epsilon < MAX_EPSILON; epsilon += EPSILON_STEP) {
            for (int minPoints = STARTING_MINPOINTS; minPoints < MAX_MINPOINTS; minPoints += MINPOINTS_STEP) {
                System.out.println("\nRunning DBSCAN...");
                timer.startChronometer();
                DBSCANAlgorithm dbscan = new DBSCANAlgorithm(points, distance_matrix, epsilon, minPoints);
                List<Cluster> clusters = dbscan.cluster();
                timer.stopChronometer();
                System.out.println("DBSCAN completed in " + timer.getElapsedTime() + " s");
                dbscan.printSummary(clusters);
            }
        }
    }

    public static void graph() {
        Dataset dataset = Bacon.buildDataset();
        Chronometer timer = new Chronometer();
        System.out.println("\nBuilding distance matrix...");
        timer.startChronometer();
        SimilarityMatrix sim = ClusteringPointsetBuilder.buildBaconSimilarityMatrix(dataset);
        double[][] distance_matrix = ClusteringPointsetBuilder.buildDistanceMatrix(sim);
        DataPoint[] points = ClusteringPointsetBuilder.createDatapoints(dataset, sim);
        timer.stopChronometer();
        System.out.println("Matrix built in " + timer.getElapsedTime() + " s");
        Configurator conf = new Configurator();
        Log log = Log.getInstance();
        log.println("minpoints, epsilon, score, noise, details");
        double STARTING_EPSILON = conf.getMinEpsilon();
        double EPSILON_STEP = conf.getEpsilonStep();
        double MAX_EPSILON = conf.getMaxEpsilon();
        int STARTING_MINPOINTS = conf.getMinMinpoints();
        int MINPOINTS_STEP = conf.getMinpointsStep();
        int MAX_MINPOINTS = conf.getMaxMinpoints();
        for (double epsilon = STARTING_EPSILON; epsilon < MAX_EPSILON; epsilon += EPSILON_STEP) {
            for (int minPoints = STARTING_MINPOINTS; minPoints < MAX_MINPOINTS; minPoints += MINPOINTS_STEP) {
                System.out.println("\nRunning DBSCAN...");
                timer.startChronometer();
                DBSCANAlgorithm dbscan = new DBSCANAlgorithm(points, distance_matrix, epsilon, minPoints);
                List<Cluster> clusters = dbscan.cluster();
                timer.stopChronometer();
                System.out.println("DBSCAN completed in " + timer.getElapsedTime() + " s");
                dbscan.printSummary(clusters);
                int score = clusters.size();
                String details = "";
                boolean there_is_noise = false;
                double noise = 0.0;
                for (Cluster c : clusters) {
                    if (Integer.parseInt(c.getLabel()) != DBSCANAlgorithm.CLUSTER_ID_NOISE) continue;
                    there_is_noise = true;
                    noise = (double)c.size() / (double)dataset.getUserCount();
                }
                int[] populations = null;
                populations = new int[score];
                int index = 0;
                for (Cluster c : clusters) {
                    if (c.getLabel().equals(String.valueOf(DBSCANAlgorithm.CLUSTER_ID_NOISE))) continue;
                    populations[index] = c.size();
                    ++index;
                }
                if (populations != null) {
                    details = "Cluster population: ";
                    for (int pop : populations) {
                        details = String.valueOf(details) + pop + " ";
                    }
                }
                log.println(String.valueOf(minPoints) + ", " + epsilon + ", " + score + ", " + Double.toString(noise * 100.0).substring(0, 5) + ", " + details);
            }
        }
    }
}

