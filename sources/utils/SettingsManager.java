package bacon.utils;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class SettingsManager {
    ResourceBundle resource = null;
    static SettingsManager theInstance = null;

    public static SettingsManager getInstance() {
        if (theInstance == null) {
            theInstance = new SettingsManager();
        }
        return theInstance;
    }

    private SettingsManager() {
        try {
            this.resource = ResourceBundle.getBundle("bacon_analyzer");
        }
        catch (MissingResourceException mre) {
            mre.printStackTrace();
        }
    }

    public String getString(String key) {
        return this.resource == null ? null : this.resource.getString(key);
    }
}

