package bacon.utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Reader;

public class MatrixHandler {
    public static boolean isSymmetric(double[][] m) {
        int dim = m[0].length;
        for (int i = 0; i < dim; ++i) {
            for (int j = 0; j < dim; ++j) {
                if (m[i][j] == m[j][i]) continue;
                return false;
            }
        }
        return true;
    }

    public static double getMax(double[][] dist) {
        int side = dist[0].length;
        double maxDist = 0.0;
        for (int row = 0; row < side; ++row) {
            for (int col = row + 1; col < side; ++col) {
                if (dist[row][col] <= maxDist) continue;
                maxDist = dist[row][col];
            }
        }
        return maxDist;
    }

    public static double getMin(double[][] dist) {
        int side = dist[0].length;
        double minDist = 1.0;
        for (int row = 0; row < side; ++row) {
            for (int col = row + 1; col < side; ++col) {
                if (dist[row][col] >= minDist) continue;
                minDist = dist[row][col];
            }
        }
        return minDist;
    }

    public static double getAverage(double[][] dist) {
        int side = dist[0].length;
        double meanDist = 0.0;
        int counter = 0;
        for (int row = 0; row < side; ++row) {
            for (int col = row + 1; col < side; ++col) {
                ++counter;
                meanDist += dist[row][col];
            }
        }
        return meanDist /= (double)counter;
    }

    public static void printSquareMatrix(double[][] a) {
        int side = a[0].length;
        for (int i = 0; i < side; ++i) {
            System.out.print("[");
            for (int j = 0; j < side; ++j) {
                if (j != side - 1) {
                    System.out.print(String.valueOf(a[i][j]) + ", ");
                    continue;
                }
                System.out.print(a[i][j]);
            }
            System.out.println("]");
        }
        System.out.println();
    }

    public static void saveMatrixOnFile(double[][] matrix, String filename) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(filename);
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        PrintStream out = new PrintStream(fos);
        int side = matrix[0].length;
        for (int i = 0; i < side; ++i) {
            out.print("[");
            for (int j = 0; j < side; ++j) {
                if (j != side - 1) {
                    out.print(String.valueOf(matrix[i][j]) + ", ");
                    continue;
                }
                out.print(matrix[i][j]);
            }
            out.println("]");
        }
    }

    public static double[][] readMatrixFromFile(String filename) {
        FileReader fr = null;
        try {
            fr = new FileReader(filename);
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        BufferedReader reader = new BufferedReader(fr);
        String line = null;
        try {
            line = reader.readLine();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        line = line.substring(1, line.length() - 1);
        String[] split = line.split(", ");
        int matrix_dim = split.length;
        double[][] matrix = new double[matrix_dim][matrix_dim];
        for (int col = 0; col < matrix_dim; ++col) {
            matrix[0][col] = Double.parseDouble(split[col]);
        }
        int row = 1;
        try {
            while ((line = reader.readLine()) != null) {
                line = line.substring(1, line.length() - 1);
                split = line.split(", ");
                for (int col = 0; col < matrix_dim; ++col) {
                    matrix[row][col] = Double.parseDouble(split[col]);
                }
                ++row;
            }
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        if (row != matrix_dim) {
            throw new RuntimeException("Wrong matrix reading.");
        }
        return matrix;
    }

    public static double[][] buildSymmetricMatrixFromUpperTriangularMatrix(double[][] uptr) {
        int i;
        int j;
        int side = uptr[0].length;
        double[][] symm = new double[side][side];
        for (i = 0; i < side; ++i) {
            for (j = i; j < side; ++j) {
                symm[i][j] = uptr[i][j];
            }
        }
        for (i = 1; i < side; ++i) {
            for (j = 0; j <= i; ++j) {
                symm[i][j] = uptr[j][i];
            }
        }
        return symm;
    }

    public int countHowManyBiggerThan(double[][] m, int dim, double th) {
        int counter = 0;
        for (int i = 0; i < dim; ++i) {
            for (int j = 0; j < dim && j >= i; ++j) {
                if (m[i][j] <= th) continue;
                ++counter;
            }
        }
        return counter;
    }

    public static int howManySmallerThan(double[][] m, double th) {
        int dim = m[0].length;
        int counter = 0;
        for (int i = 0; i < dim; ++i) {
            for (int j = i + 1; j < dim; ++j) {
                if (m[i][j] >= th) continue;
                ++counter;
            }
        }
        return counter;
    }

    public boolean isAllGreaterThanZero(double[][] a, int side) {
        for (int i = 0; i < side; ++i) {
            for (int j = 0; j < side; ++j) {
                if (a[i][j] >= 0.0) continue;
                return false;
            }
        }
        return true;
    }

    public boolean isThereNan(double[][] a, int side) {
        for (int i = 0; i < side; ++i) {
            for (int j = 0; j < side; ++j) {
                if (!Double.valueOf(a[i][j]).isNaN()) continue;
                return true;
            }
        }
        return false;
    }

    public double[][] buildDistanceMatrixFromSimilarityMatrix(double[][] sim, int side) {
        double[][] dist = new double[side][side];
        for (int i = 0; i < side; ++i) {
            for (int j = 0; j < side; ++j) {
                dist[i][j] = Double.parseDouble("1") - sim[i][j];
            }
        }
        return dist;
    }

    public double getDistanceValueFromUpperTriangularSimilarityMatrix(double[][] sim, int side, int row, int col) {
        double result = 0.0;
        result = row == col ? 1.0 - sim[row][row] : (row < col ? 1.0 - sim[row][col] : 1.0 - sim[col][row]);
        return result;
    }
}

