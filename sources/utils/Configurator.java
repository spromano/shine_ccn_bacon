package bacon.utils;

import bacon.utils.Log;
import bacon.utils.SettingsManager;

public class Configurator {
    private SettingsManager settings = SettingsManager.getInstance();

    public boolean getLogVerboseMode() {
        return Boolean.parseBoolean(this.settings.getString("settings.bacon_analyzer.log.verbose"));
    }

    public String getDataMode() {
        return this.settings.getString("settings.bacon_analyzer.data.mode");
    }

    public double getMusicVotesPercentage() {
        return Double.parseDouble(this.settings.getString("settings.bacon_analyzer.data.music.votes_percentage"));
    }

    public int getUserScaleFactor() {
        return Integer.parseInt(this.settings.getString("settings.bacon_analyzer.data.artificial_1.users.scalefactor"));
    }

    public int getItemScaleFactor() {
        return Integer.parseInt(this.settings.getString("settings.bacon_analyzer.data.artificial_1.items.scalefactor"));
    }

    public int getNumGroup() {
        return Integer.parseInt(this.settings.getString("settings.bacon_analyzer.data.artificial_1.groups"));
    }

    public double getVotesPercentage() {
        return Double.parseDouble(this.settings.getString("settings.bacon_analyzer.data.artificial_1.votes_percentage"));
    }

    public int getUserScaleFactor_2() {
        return Integer.parseInt(this.settings.getString("settings.bacon_analyzer.data.artificial_2.users.scalefactor"));
    }

    public int getItemScaleFactor_2() {
        return Integer.parseInt(this.settings.getString("settings.bacon_analyzer.data.artificial_2.items.scalefactor"));
    }

    public double getRandomVotesPercentage() {
        return Double.parseDouble(this.settings.getString("settings.bacon_analyzer.data.artificial_2.random_votes_percentage"));
    }

    public String getDBName() {
        return this.settings.getString("settings.bacon_analyzer.data.db.name");
    }

    public String getHome() {
        return this.settings.getString("settings.bacon_analyzer.home");
    }

    public Log getAnalyzerLogFile() {
        Log log = Log.getInstance();
        return log;
    }

    public boolean getArtificialDataTest() {
        return Boolean.parseBoolean(this.settings.getString("settings.bacon_analyzer.data.artificial.test"));
    }

    public boolean enabledValidation() {
        return Boolean.parseBoolean(this.settings.getString("settings.bacon_analyzer.performance"));
    }

    public int getMinMinpoints() {
        return Integer.parseInt(this.settings.getString("settings.bacon_analyzer.dbscan.minpoints.min"));
    }

    public int getMaxMinpoints() {
        return Integer.parseInt(this.settings.getString("settings.bacon_analyzer.dbscan.minpoints.max"));
    }

    public int getMinpointsStep() {
        return Integer.parseInt(this.settings.getString("settings.bacon_analyzer.dbscan.minpoints.step"));
    }

    public Double getMinEpsilon() {
        return Double.parseDouble(this.settings.getString("settings.bacon_analyzer.dbscan.epsilon.min"));
    }

    public Double getMaxEpsilon() {
        return Double.parseDouble(this.settings.getString("settings.bacon_analyzer.dbscan.epsilon.max"));
    }

    public Double getEpsilonStep() {
        return Double.parseDouble(this.settings.getString("settings.bacon_analyzer.dbscan.epsilon.step"));
    }

    public String[] getDbscanParameters() {
        String[] parameters = new String[]{new String(this.settings.getString("settings.bacon_analyzer.dbscan.epsilon." + this.getDataMode())), new String(this.settings.getString("settings.bacon_analyzer.dbscan.minpoints." + this.getDataMode()))};
        return parameters;
    }

    public double getDbscanEpsilon() {
        return Double.valueOf(this.settings.getString("settings.bacon_analyzer.dbscan.epsilon"));
    }

    public int getDbscanMinpoints() {
        return Integer.valueOf(this.settings.getString("settings.bacon_analyzer.dbscan.minpoints"));
    }

    public String getMovielensDataDir() {
        return this.settings.getString("settings.bacon_analyzer.data.movielens.dir");
    }

    public String getTestMode() {
        return this.settings.getString("settings.bacon_analyzer.test");
    }
}

