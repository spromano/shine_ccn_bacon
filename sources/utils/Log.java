package bacon.utils;

import bacon.utils.SettingsManager;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Date;

public class Log {
    private SettingsManager settings = SettingsManager.getInstance();
    private String logFile = this.settings.getString("settings.bacon_analyzer.log.filename");
    private PrintWriter writer;
    private boolean verbose;
    static Log theInstance = null;

    public static Log getInstance() {
        if (theInstance == null) {
            theInstance = new Log();
        }
        return theInstance;
    }

    private Log() {
        try {
            this.writer = new PrintWriter(new FileOutputStream(this.logFile, true), true);
            this.writer.println("\n\n******* [" + new Date() + "] --- BACON Analyzer Logs *******");
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String getLogFile() {
        return this.logFile;
    }

    public void print(String line) {
        this.writer.print(line);
    }

    public void println(String line) {
        this.writer.println(line);
    }

    public void printIfVerbose(String line) {
        if (this.verbose) {
            this.writer.println(line);
        }
    }

    public void setVerboseMode(boolean mode) {
        this.verbose = mode;
    }

    public void setVerboseMode() {
        this.verbose = true;
    }

    public void unverbose() {
        this.verbose = false;
    }
}

