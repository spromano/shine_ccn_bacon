package bacon.utils;

import java.io.PrintStream;

public class Chronometer {
    private long startTime = Long.parseLong("0");
    private long stopTime = Long.parseLong("0");
    private boolean active = false;
    private String name;

    public Chronometer() {
        this.name = "chronometer_" + this.toString();
    }

    public Chronometer(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return this.active;
    }

    public synchronized void startChronometer() {
        if (!this.active) {
            this.startTime = System.currentTimeMillis();
            this.active = true;
        }
    }

    public synchronized void stopChronometer() {
        if (this.active) {
            this.stopTime = System.currentTimeMillis();
            this.active = false;
        }
    }

    public long getElapsedTime_Millis() {
        if (this.active) {
            return Long.parseLong("-1");
        }
        return this.stopTime - this.startTime;
    }

    public long getElapsedTime() {
        return this.getElapsedTime_Millis() / 1000L;
    }

    public void printElapsedTime() {
        if (this.active) {
            System.out.println("Not ready, still measuring...");
        } else {
            System.out.println("Elapsed time: " + this.getElapsedTime() + " s");
        }
    }

    public String whoAmI() {
        return this.name;
    }
}

