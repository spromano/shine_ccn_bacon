package bacon.clustering;

import bacon.similarity.BaconUserBasedSimilarity;
import bacon.utils.MatrixHandler;
import iweb2.ch3.collaborative.model.Dataset;
import iweb2.ch3.collaborative.model.Rating;
import iweb2.ch3.collaborative.model.User;
import iweb2.ch3.collaborative.similarity.naive.SimilarityMatrix;
import iweb2.ch4.model.Attribute;
import iweb2.ch4.model.DataPoint;
import iweb2.ch4.similarity.JaccardCoefficient;
import java.io.PrintStream;
import java.util.Collection;

public class ClusteringPointsetBuilder {
    public static DataPoint[] createJaccardDatapoints(Dataset dataset) {
        int num_points = dataset.getUserCount();
        DataPoint[] points = new DataPoint[num_points];
        int i = 0;
        int attributes_num = 0;
        int max_attributes_num = Integer.MIN_VALUE;
        int min_attributes_num = Integer.MAX_VALUE;
        for (User u : dataset.getUsers()) {
            String label = String.valueOf(u.getName()) + "_" + u.getId();
            Attribute[] voted_item_attributes = ClusteringPointsetBuilder.buildJaccardAttributes(u);
            int num_voted_item = voted_item_attributes.length;
            points[i] = new DataPoint(label, voted_item_attributes);
            if (num_voted_item > max_attributes_num) {
                max_attributes_num = num_voted_item;
            } else if (num_voted_item < min_attributes_num) {
                min_attributes_num = num_voted_item;
            }
            ++i;
            attributes_num += num_voted_item;
        }
        double mean_attributes_num = (double)attributes_num / (double)points.length;
        System.out.println("\nDatapoints info");
        System.out.println("# points = " + points.length);
        System.out.println("average attributes length / mean voted items = " + mean_attributes_num);
        System.out.println("min attributes length / min # voted items = " + min_attributes_num);
        System.out.println("max attributes length / max # voted items = " + max_attributes_num);
        return points;
    }

    private static Attribute[] buildJaccardAttributes(User u) {
        int num_item = u.getAllRatings().size();
        Attribute[] voted_items = new Attribute[num_item];
        int i = 0;
        for (Rating r : u.getAllRatings()) {
            voted_items[i] = new Attribute(String.valueOf(r.getItemId()), String.valueOf(r.getItemId()));
            ++i;
        }
        if (i != num_item) {
            throw new RuntimeException("Wrong attributes construction.");
        }
        return voted_items;
    }

    public static double[][] buildJaccardDistanceMatrix(DataPoint[] points) {
        int matrix_dim = points.length;
        double[][] distance = new double[matrix_dim][matrix_dim];
        JaccardCoefficient jaccard = new JaccardCoefficient();
        for (int i = 0; i < matrix_dim; ++i) {
            for (int j = i + 1; j < matrix_dim; ++j) {
                distance[i][j] = 1.0 - jaccard.similarity(points[i].getTextAttrValues(), points[j].getTextAttrValues());
                distance[j][i] = distance[i][j];
            }
            distance[i][i] = 0.0;
        }
        System.out.println("\nMatrix info");
        System.out.println("Symmetric = " + MatrixHandler.isSymmetric(distance));
        System.out.println("Min value = " + MatrixHandler.getMin(distance));
        System.out.println("Max value = " + MatrixHandler.getMax(distance));
        System.out.println("Average value = " + MatrixHandler.getAverage(distance));
        return distance;
    }

    public static double[][] buildJaccardSimilarityMatrix(DataPoint[] points) {
        int matrix_dim = points.length;
        double[][] similarity = new double[matrix_dim][matrix_dim];
        JaccardCoefficient jaccard = new JaccardCoefficient();
        for (int i = 0; i < matrix_dim; ++i) {
            for (int j = i + 1; j < matrix_dim; ++j) {
                similarity[i][j] = jaccard.similarity(points[i].getTextAttrValues(), points[j].getTextAttrValues());
                similarity[j][i] = similarity[i][j];
            }
            similarity[i][i] = 1.0;
        }
        System.out.println("\nMatrix info");
        System.out.println("Symmetric = " + MatrixHandler.isSymmetric(similarity));
        System.out.println("Min value = " + MatrixHandler.getMin(similarity));
        System.out.println("Max value = " + MatrixHandler.getMax(similarity));
        System.out.println("Average value = " + MatrixHandler.getAverage(similarity));
        return similarity;
    }

    public static DataPoint[] createDatapoints(Dataset dataset, SimilarityMatrix sim) {
        int num_points = dataset.getUserCount();
        DataPoint[] points = new DataPoint[num_points];
        for (int i = 0; i < num_points; ++i) {
            User u = dataset.getUser(sim.getObjIdFromIndex(i));
            String label = String.valueOf(u.getName()) + "_" + u.getId();
            Attribute[] attributes = ClusteringPointsetBuilder.buildMatrixIndexAttributes(i);
            points[i] = new DataPoint(label, attributes);
        }
        return points;
    }

    public static DataPoint[] createDatapoints(Dataset dataset) {
        int num_points = dataset.getUserCount();
        DataPoint[] points = new DataPoint[num_points];
        for (User u : dataset.getUsers()) {
            String label = String.valueOf(u.getName()) + "_" + u.getId();
            Attribute[] attributes = ClusteringPointsetBuilder.buildMatrixIndexAttributes(u.getId() - 1);
            points[u.getId() - 1] = new DataPoint(label, attributes);
        }
        return points;
    }

    public static double[][] buildDistanceMatrix(SimilarityMatrix sim) {
        int num_points = sim.getSimilarityMatrix()[0].length;
        double[][] dist = new double[num_points][num_points];
        for (int i = 0; i < num_points; ++i) {
            for (int j = i + 1; j < num_points; ++j) {
                dist[i][j] = 1.0 - sim.getSimilarityMatrix()[i][j];
                dist[j][i] = dist[i][j];
            }
            dist[i][i] = 0.0;
        }
        System.out.println("\nMatrix info");
        System.out.println("Symmetric = " + MatrixHandler.isSymmetric(dist));
        System.out.println("Min value = " + MatrixHandler.getMin(dist));
        System.out.println("Max value = " + MatrixHandler.getMax(dist));
        System.out.println("Average value = " + MatrixHandler.getAverage(dist));
        return dist;
    }

    private static Attribute[] buildMatrixIndexAttributes(int index) {
        return new Attribute[]{new Attribute("matrix_index", String.valueOf(index))};
    }

    public static SimilarityMatrix buildBaconSimilarityMatrix(Dataset dataset) {
        BaconUserBasedSimilarity sim = new BaconUserBasedSimilarity(dataset);
        return sim;
    }
}

