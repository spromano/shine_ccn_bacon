package bacon.clustering;

import bacon.clustering.DBSCANAlgorithm;
import bacon.model.CommunityDetector;
import bacon.utils.Configurator;
import iweb2.ch4.model.Cluster;
import iweb2.ch4.model.DataPoint;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DBSCANCommunityDetector
extends CommunityDetector {
    private DBSCANAlgorithm dbscan;
    private List<Cluster> clusters = null;
    private Map<Integer, DataPoint> typical_points = null;

    public DBSCANCommunityDetector(DataPoint[] datapoints, double[][] dist_matrix) {
        super(datapoints, dist_matrix);
        String[] dbscan_parameters = this.performParametersTuning();
        this.dbscan = new DBSCANAlgorithm(datapoints, dist_matrix, Double.parseDouble(dbscan_parameters[0]), Integer.parseInt(dbscan_parameters[1]));
    }

    private String[] performParametersTuning() {
        Configurator conf = new Configurator();
        String[] parameters = conf.getDbscanParameters();
        return parameters;
    }

    public DBSCANCommunityDetector(DataPoint[] datapoints, double[][] dist_matrix, double epsilon, int minpoints) {
        super(datapoints, dist_matrix);
        this.dbscan = new DBSCANAlgorithm(datapoints, dist_matrix, epsilon, minpoints);
    }

    @Override
    protected List<Cluster> cluster() {
        this.clusters = this.dbscan.cluster();
        return this.clusters;
    }

    @Override
    public Map<Integer, DataPoint> getTypicalDataPoints() {
        if (this.clusters != null) {
            this.typical_points = new LinkedHashMap<Integer, DataPoint>();
            Map<String, Integer> eps_nbs_map = this.dbscan.getEpsNbsMap();
            int max_nbs_num = -1;
            DataPoint typical_point = null;
            for (Cluster c : this.clusters) {
                for (DataPoint p : c.getElements()) {
                    if (eps_nbs_map.get(p.getLabel()) <= max_nbs_num) continue;
                    typical_point = p;
                }
                this.typical_points.put(Integer.parseInt(c.getLabel()), typical_point);
            }
            return this.typical_points;
        }
        return null;
    }

    @Override
    public void printDetectionSummary() {
        this.dbscan.printSummary(this.clusters);
    }

    @Override
    public void printResults() {
        this.dbscan.printResults(this.clusters);
    }

    @Override
    public void printResultsOnLogFile() {
    }
}

