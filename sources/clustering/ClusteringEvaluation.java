package bacon.clustering;

import iweb2.ch4.model.Attribute;
import iweb2.ch4.model.Cluster;
import iweb2.ch4.model.DataPoint;
import java.io.PrintStream;
import java.util.List;
import java.util.Set;

public class ClusteringEvaluation {
    private List<Cluster> communities;
    private double[][] distance_matrix;

    public ClusteringEvaluation(List<Cluster> communities, double[][] dist_matrix) {
        this.communities = communities;
        this.distance_matrix = dist_matrix;
    }

    public double getSingleLinkDistance(Cluster c1, Cluster c2) {
        double min_D;
        double temp = min_D = Double.MAX_VALUE;
        for (DataPoint d1 : c1.getElements()) {
            int index_d1 = Integer.parseInt(d1.getAttributes()[0].getTextValue());
            for (DataPoint d2 : c2.getElements()) {
                int index_d2 = Integer.parseInt(d2.getAttributes()[0].getTextValue());
                temp = this.distance_matrix[index_d1][index_d2];
                if (temp >= min_D) continue;
                min_D = temp;
            }
        }
        return min_D;
    }

    public double getAverageLinkDistance(Cluster c1, Cluster c2) {
        double av_D = 0.0;
        int counter = 0;
        for (DataPoint d1 : c1.getElements()) {
            int index_d1 = Integer.parseInt(d1.getAttributes()[0].getTextValue());
            for (DataPoint d2 : c2.getElements()) {
                int index_d2 = Integer.parseInt(d2.getAttributes()[0].getTextValue());
                ++counter;
                av_D += this.distance_matrix[index_d1][index_d2];
            }
        }
        return av_D / (double)counter;
    }

    public double getCompleteLinkDistance(Cluster c1, Cluster c2) {
        double max_D = -1.0;
        double temp = -1.0;
        for (DataPoint d1 : c1.getElements()) {
            int index_d1 = Integer.parseInt(d1.getAttributes()[0].getTextValue());
            for (DataPoint d2 : c2.getElements()) {
                int index_d2 = Integer.parseInt(d2.getAttributes()[0].getTextValue());
                temp = this.distance_matrix[index_d1][index_d2];
                if (temp <= max_D) continue;
                max_D = temp;
            }
        }
        return max_D;
    }

    public double getAverageInternalDistance(Cluster c) {
        int counter = 0;
        double av_D = 0.0;
        for (DataPoint d1 : c.getElements()) {
            for (DataPoint d2 : c.getElements()) {
                if (d1.getLabel().equals(d2.getLabel())) continue;
                ++counter;
                av_D += this.distance_matrix[Integer.parseInt(d1.getAttributes()[0].getTextValue())][Integer.parseInt(d2.getAttributes()[0].getTextValue())];
            }
        }
        return av_D / (double)counter;
    }

    public double getMaxInternalDistance(Cluster c) {
        double max_D = -1.0;
        double temp = -1.0;
        for (DataPoint d1 : c.getElements()) {
            for (DataPoint d2 : c.getElements()) {
                if (d1.getLabel().equals(d2.getLabel()) || (temp = this.distance_matrix[Integer.parseInt(d1.getAttributes()[0].getTextValue())][Integer.parseInt(d2.getAttributes()[0].getTextValue())]) <= max_D) continue;
                max_D = temp;
            }
        }
        return max_D;
    }

    public double getMinInternalDistance(Cluster c) {
        double min_D = 2.0;
        double temp = 2.0;
        for (DataPoint d1 : c.getElements()) {
            for (DataPoint d2 : c.getElements()) {
                if (d1.getLabel().equals(d2.getLabel()) || (temp = this.distance_matrix[Integer.parseInt(d1.getAttributes()[0].getTextValue())][Integer.parseInt(d2.getAttributes()[0].getTextValue())]) >= min_D) continue;
                min_D = temp;
            }
        }
        return min_D;
    }

    public void printClusterProperties() {
        System.out.println("\n*** Cluster properties (" + this.communities.size() + " discovered clusters) ***");
        for (Cluster c : this.communities) {
            System.out.println("--- CLUSTER " + c.getLabel());
            int clusterSize = c.size();
            System.out.println(" size: " + clusterSize);
            System.out.println(" average internal distance: " + this.getAverageInternalDistance(c));
            System.out.println(" max internal distance: " + this.getMaxInternalDistance(c));
            for (Cluster other_c : this.communities) {
                if (c.getLabel().equals(other_c.getLabel())) continue;
                System.out.println(" distance from cluster " + other_c.getLabel() + ": ");
                System.out.println("  single link = " + this.getSingleLinkDistance(c, other_c));
                System.out.println("  average link = " + this.getAverageLinkDistance(c, other_c));
                System.out.println("  complete link = " + this.getCompleteLinkDistance(c, other_c));
            }
        }
        System.out.println("*** End ***");
    }
}

