package bacon.clustering;

import bacon.clustering.ClusteringPointsetBuilder;
import bacon.data.ArtificialDataLoader_Jaccard;
import iweb2.ch3.collaborative.model.Dataset;
import iweb2.ch4.model.Cluster;
import iweb2.ch4.model.DataPoint;
import iweb2.ch4.similarity.Distance;
import iweb2.ch4.utils.ObjectToIndexMapping;
import iweb2.ch4.utils.TermFrequencyBuilder;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DBSCANAlgorithm {
    private DataPoint[] points;
    private double[][] adjacencyMatrix;
    private double eps;
    public static int CLUSTER_ID_NOISE = -1;
    private int CLUSTER_ID_UNCLASSIFIED = 0;
    private int nextClusterId = 1;
    private Map<Integer, Set<DataPoint>> clusters = new LinkedHashMap<Integer, Set<DataPoint>>();
    private int minPoints;
    private ObjectToIndexMapping<DataPoint> idxMapping = new ObjectToIndexMapping();
    private Map<String, Integer> eps_nbs_map = new LinkedHashMap<String, Integer>();
    private boolean verbose = true;

    public boolean isVerbose() {
        return this.verbose;
    }

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    public DBSCANAlgorithm(DataPoint[] points, double[][] adjacencyMatrix, double eps, int minPoints) {
        this.init(points, eps, minPoints);
        this.adjacencyMatrix = adjacencyMatrix;
    }

    public DBSCANAlgorithm(DataPoint[] points, Distance distance, double eps, int minPoints, boolean useTermFrequencies) {
        this.init(points, eps, minPoints);
        this.adjacencyMatrix = DBSCANAlgorithm.calculateAdjacencyMatrix(distance, points, useTermFrequencies);
    }

    private static double[][] calculateAdjacencyMatrix(Distance distance, DataPoint[] points, boolean useTermFrequencies) {
        int n = points.length;
        double[][] a = new double[n][n];
        for (int i = 0; i < n; ++i) {
            double[] x = points[i].getNumericAttrValues();
            for (int j = i + 1; j < n; ++j) {
                double[] y;
                if (useTermFrequencies) {
                    double[][] tfVectors = TermFrequencyBuilder.buildTermFrequencyVectors(points[i].getTextAttrValues(), points[j].getTextAttrValues());
                    x = tfVectors[0];
                    y = tfVectors[1];
                } else {
                    y = points[j].getNumericAttrValues();
                }
                a[i][j] = distance.getDistance(x, y);
                a[j][i] = a[i][j];
            }
            a[i][i] = 0.0;
        }
        return a;
    }

    private void init(DataPoint[] points, double neighborThreshold, int minPoints) {
        this.points = points;
        this.eps = neighborThreshold;
        this.minPoints = minPoints;
        for (DataPoint p : points) {
            this.idxMapping.getIndex(p);
            this.eps_nbs_map.put(p.getLabel(), -1);
            this.assignPointToCluster(p, this.CLUSTER_ID_UNCLASSIFIED);
        }
    }

    public List<Cluster> cluster() {
        int clusterId = this.getNextClusterId();
        for (DataPoint p : this.points) {
            boolean isClusterCreated;
            if (!this.isUnclassified(p) || !(isClusterCreated = this.createCluster(p, clusterId))) continue;
            clusterId = this.getNextClusterId();
        }
        ArrayList<Cluster> allClusters = new ArrayList<Cluster>();
        for (Map.Entry<Integer, Set<DataPoint>> e : this.clusters.entrySet()) {
            String label = String.valueOf(e.getKey());
            Set<DataPoint> points = e.getValue();
            if (points == null || points.isEmpty()) continue;
            Cluster cluster = new Cluster(label, (Collection<DataPoint>)e.getValue());
            allClusters.add(cluster);
        }
        return allClusters;
    }

    private boolean createCluster(DataPoint p, Integer clusterId) {
        boolean isClusterCreated = false;
        Set<DataPoint> nPoints = this.findNeighbors(p, this.eps);
        if (nPoints.size() < this.minPoints) {
            this.assignPointToCluster(p, CLUSTER_ID_NOISE);
            isClusterCreated = false;
        } else {
            this.assignPointToCluster(nPoints, (int)clusterId);
            nPoints.remove(p);
            while (nPoints.size() > 0) {
                DataPoint nPoint = nPoints.iterator().next();
                Set<DataPoint> nnPoints = this.findNeighbors(nPoint, this.eps);
                if (nnPoints.size() >= this.minPoints) {
                    for (DataPoint nnPoint : nnPoints) {
                        if (this.isNoise(nnPoint)) {
                            this.assignPointToCluster(nnPoint, (int)clusterId);
                            continue;
                        }
                        if (!this.isUnclassified(nnPoint)) continue;
                        nPoints.add(nnPoint);
                        this.assignPointToCluster(nnPoint, (int)clusterId);
                    }
                }
                nPoints.remove(nPoint);
            }
            isClusterCreated = true;
        }
        return isClusterCreated;
    }

    private Set<DataPoint> findNeighbors(DataPoint p, double threshold) {
        HashSet<DataPoint> neighbors = new HashSet<DataPoint>();
        int i = this.idxMapping.getIndex(p);
        int n = this.idxMapping.getSize();
        for (int j = 0; j < n; ++j) {
            if (this.adjacencyMatrix[i][j] > threshold) continue;
            neighbors.add(this.idxMapping.getObject(j));
        }
        if (this.eps_nbs_map.get(p.getLabel()) == Integer.valueOf(-1)) {
            this.eps_nbs_map.put(p.getLabel(), neighbors.size());
        }
        return neighbors;
    }

    private boolean isUnclassified(DataPoint p) {
        return this.isPointInCluster(p, this.CLUSTER_ID_UNCLASSIFIED);
    }

    private boolean isNoise(DataPoint p) {
        return this.isPointInCluster(p, CLUSTER_ID_NOISE);
    }

    private boolean isPointInCluster(DataPoint p, int clusterId) {
        boolean inCluster = false;
        Set<DataPoint> points = this.clusters.get(clusterId);
        if (points != null) {
            inCluster = points.contains(p);
        }
        return inCluster;
    }

    private void assignPointToCluster(Set<DataPoint> points, int clusterId) {
        for (DataPoint p : points) {
            this.assignPointToCluster(p, clusterId);
        }
    }

    private void assignPointToCluster(DataPoint p, int clusterId) {
        if (this.isNoise(p)) {
            this.removePointFromCluster(p, CLUSTER_ID_NOISE);
        } else if (this.isUnclassified(p)) {
            this.removePointFromCluster(p, this.CLUSTER_ID_UNCLASSIFIED);
        } else if (clusterId != this.CLUSTER_ID_UNCLASSIFIED) {
            System.out.println("\n warning: suppressed runtime exception while trying to move point " + this.idxMapping.getIndex(p) + " in cluster " + clusterId + " -- point already assigned to some other cluster");
            return;
        }
        Set<DataPoint> points = this.clusters.get(clusterId);
        if (points == null) {
            points = new HashSet<DataPoint>();
            this.clusters.put(clusterId, points);
        }
        points.add(p);
    }

    private boolean removePointFromCluster(DataPoint p, int clusterId) {
        boolean removed = false;
        Set<DataPoint> points = this.clusters.get(clusterId);
        if (points != null) {
            removed = points.remove(p);
        }
        return removed;
    }

    private int getNextClusterId() {
        return this.nextClusterId++;
    }

    public void printDistances() {
        System.out.println("Point Similarity matrix:");
        for (int i = 0; i < this.adjacencyMatrix.length; ++i) {
            System.out.println(Arrays.toString(this.adjacencyMatrix[i]));
        }
    }

    public void printResults(List<Cluster> allClusters) {
        System.out.println("DBSCAN Clustering with NeighborThreshold=" + this.eps + " minPoints=" + this.minPoints);
        System.out.println("Clusters:");
        String noiseElements = "no noise elements";
        for (Cluster c : allClusters) {
            if (String.valueOf(CLUSTER_ID_NOISE).equals(c.getLabel())) {
                noiseElements = c.getElementsAsString();
                continue;
            }
            System.out.println("____________________________________________________________\n");
            System.out.println(String.valueOf(c.getLabel()) + ": \n" + c.getElementsAsString());
            System.out.println("____________________________________________________________\n\n");
        }
        System.out.println("Noise Elements:\n " + noiseElements);
        System.out.println();
    }

    public void printSummary(List<Cluster> allClusters) {
        System.out.println("\nDBSCAN Clustering Summary");
        System.out.println("Parameters: epsilon = " + this.eps + ",  minPoints=" + this.minPoints);
        System.out.println("# clusters = " + allClusters.size());
        boolean noiseElements = false;
        int num_noise = 0;
        for (Cluster c : allClusters) {
            if (String.valueOf(CLUSTER_ID_NOISE).equals(c.getLabel())) {
                noiseElements = true;
                num_noise = c.getElements().size();
                continue;
            }
            System.out.println("Cluster " + c.getLabel() + ", # points = " + c.getElements().size());
        }
        if (noiseElements) {
            System.out.println("Noise Cluster, # points = " + num_noise);
        }
        System.out.println();
    }

    public Map<String, Integer> getEpsNbsMap() {
        return this.eps_nbs_map;
    }

    public static void main(String[] args) {
        ArtificialDataLoader_Jaccard loader = new ArtificialDataLoader_Jaccard(1, 3);
        Dataset dataset = loader.loadDataset();
        DataPoint[] points = ClusteringPointsetBuilder.createJaccardDatapoints(dataset);
        double[][] jaccard_distance_matrix = ClusteringPointsetBuilder.buildJaccardDistanceMatrix(points);
        double threshold = 0.8;
        int minPoints = 2;
        DBSCANAlgorithm dbscan = new DBSCANAlgorithm(points, jaccard_distance_matrix, threshold, minPoints);
        dbscan.cluster();
    }
}

