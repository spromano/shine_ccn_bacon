package bacon.similarity;

import iweb2.ch2.webcrawler.utils.ValueToIndexMapping;
import iweb2.ch3.collaborative.model.Dataset;
import iweb2.ch3.collaborative.model.Rating;
import iweb2.ch3.collaborative.model.User;
import iweb2.ch3.collaborative.similarity.naive.SimilarityMatrixImpl;
import iweb2.ch3.collaborative.similarity.util.RatingCountMatrix;
import java.util.Collection;

public class BaconUserBasedSimilarity
extends SimilarityMatrixImpl {
    private static final long serialVersionUID = 5741618250320567238L;

    public BaconUserBasedSimilarity(Dataset dataSet) {
        this(BaconUserBasedSimilarity.class.getSimpleName(), dataSet, true);
    }

    public BaconUserBasedSimilarity(String id, Dataset dataSet, boolean keepRatingCountMatrix) {
        this.id = id;
        this.keepRatingCountMatrix = keepRatingCountMatrix;
        this.useObjIdToIndexMapping = dataSet.isIdMappingRequired();
        this.calculate(dataSet);
    }

    @Override
    protected void calculate(Dataset dataSet) {
        int nUsers = dataSet.getUserCount();
        int nRatingValues = 5;
        this.similarityValues = new double[nUsers][nUsers];
        if (this.keepRatingCountMatrix) {
            this.ratingCountMatrix = new RatingCountMatrix[nUsers][nUsers];
        }
        if (this.useObjIdToIndexMapping) {
            for (User u : dataSet.getUsers()) {
                this.idMapping.getIndex(String.valueOf(u.getId()));
            }
        }
        for (int u = 0; u < nUsers; ++u) {
            int userAId = this.getObjIdFromIndex(u);
            User userA = dataSet.getUser(userAId);
            for (int v = u + 1; v < nUsers; ++v) {
                int userBId = this.getObjIdFromIndex(v);
                User userB = dataSet.getUser(userBId);
                RatingCountMatrix rcm = new RatingCountMatrix(userA, userB, nRatingValues);
                int intersectionCount = rcm.getTotalCount();
                int unionCount = userA.getAllRatings().size() + userB.getAllRatings().size() - intersectionCount;
                int agreementCount = rcm.getAgreementCount();
                this.similarityValues[u][v] = agreementCount > 0 ? (double)agreementCount / (double)unionCount : 0.0;
                if (!this.keepRatingCountMatrix) continue;
                this.ratingCountMatrix[u][v] = rcm;
            }
            this.similarityValues[u][u] = 1.0;
        }
    }
}

