package bacon.data;

import iweb2.ch3.collaborative.model.Dataset;

public interface DataLoader {
    public Dataset loadDataset();
}

