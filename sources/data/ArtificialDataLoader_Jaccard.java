package bacon.data;

import bacon.data.DataLoader;
import iweb2.ch3.collaborative.data.BaseDataset;
import iweb2.ch3.collaborative.data.RatingBuilder;
import iweb2.ch3.collaborative.model.Dataset;
import iweb2.ch3.collaborative.model.Item;
import iweb2.ch3.collaborative.model.Rating;
import iweb2.ch3.collaborative.model.User;
import java.io.PrintStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class ArtificialDataLoader_Jaccard
implements DataLoader {
    public static final String[] PARTIAL_IDS = new String[]{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    public static int PARTIAL_IDS_LENGTH = PARTIAL_IDS.length;
    public static final int LOW_RATING = 1;
    public static final int HIGH_RATING = 5;
    public static double RANDOM_VOTES_PERCENTAGE_DEFAULT = 0.0;
    public static int MAX_SCALE_FACTOR = 1000;
    public int numUsers;
    public int numItems;
    public int numGroups;
    public int numRatings;
    public Dataset dataset;

    public ArtificialDataLoader_Jaccard(int userCloneFactor, int itemCloneFactor) {
        if (!this.checkParameters(userCloneFactor, itemCloneFactor, RANDOM_VOTES_PERCENTAGE_DEFAULT)) {
            throw new RuntimeException("ERR - Cannot build artificial dataset. Unacceptable parameters.");
        }
        this.numUsers = userCloneFactor * PARTIAL_IDS_LENGTH;
        this.numItems = itemCloneFactor * PARTIAL_IDS_LENGTH;
        this.dataset = this.createDataset();
        this.numRatings = this.dataset.getRatingsCount();
        System.out.println("\nArtificial_2 dataset properties:");
        System.out.println("# users = " + this.numUsers);
        System.out.println("# items = " + this.numItems);
        System.out.println("# ratings = " + this.numRatings);
        System.out.println("% of random votes per user = " + RANDOM_VOTES_PERCENTAGE_DEFAULT);
    }

    public ArtificialDataLoader_Jaccard(int userCloneFactor, int itemCloneFactor, double randomVotePercentage) {
        if (!this.checkParameters(userCloneFactor, itemCloneFactor, randomVotePercentage)) {
            throw new RuntimeException("ERR - Cannot build artificial dataset. Unacceptable parameters.");
        }
        this.numUsers = userCloneFactor * PARTIAL_IDS_LENGTH;
        this.numItems = itemCloneFactor * PARTIAL_IDS_LENGTH;
        this.dataset = this.createDataset(randomVotePercentage);
        this.numRatings = this.dataset.getRatingsCount();
        System.out.println("\nArtificial_2 dataset properties:");
        System.out.println("# users = " + this.numUsers);
        System.out.println("# items = " + this.numItems);
        System.out.println("# ratings = " + this.numRatings);
        System.out.println("% of random votes per user = " + randomVotePercentage);
    }

    private boolean checkParameters(int u, int i, double p) {
        return u > 0 && u <= MAX_SCALE_FACTOR && i > 0 && i < MAX_SCALE_FACTOR && p >= 0.0 && p <= 1.0;
    }

    @Override
    public Dataset loadDataset() {
        return this.dataset;
    }

    private Dataset createDataset() {
        return this.createDataset(RANDOM_VOTES_PERCENTAGE_DEFAULT);
    }

    private Dataset createDataset(double percentOfRandomItem) {
        BaseDataset ds = new BaseDataset();
        Item[] allItems = this.loadAllItems();
        String[] userLabels = this.generateUserLabels();
        for (int i = 0; i < this.numUsers; ++i) {
            int userId = i;
            String userName = userLabels[i];
            int lowRating = 1;
            int highRating = 5;
            int class_case = Integer.MIN_VALUE;
            if (userName.toLowerCase().charAt(0) <= 'b') {
                class_case = 1;
            }
            if (userName.toLowerCase().charAt(0) > 'b' && userName.toLowerCase().charAt(0) <= 'd') {
                class_case = 2;
            }
            if (userName.toLowerCase().charAt(0) > 'd' && userName.toLowerCase().charAt(0) <= 'h') {
                class_case = 3;
            }
            if (userName.toLowerCase().charAt(0) > 'h' && userName.toLowerCase().charAt(0) <= 'z') {
                class_case = 4;
            }
            switch (class_case) {
                case 1: {
                    lowRating = 5;
                    highRating = 5;
                    break;
                }
                case 2: {
                    lowRating = 4;
                    highRating = 4;
                    break;
                }
                case 3: {
                    lowRating = 1;
                    highRating = 1;
                    break;
                }
                case 4: {
                    lowRating = 2;
                    highRating = 3;
                }
            }
            Item[] items = this.pickRandomItems(allItems, percentOfRandomItem, class_case);
            RatingBuilder ratingBuider = new RatingBuilder();
            List<Rating> ratings = ratingBuider.createBiasedRatings(userId, items, lowRating, highRating);
            User bu = new User(userId, userName, ratings);
            ds.add(bu);
        }
        return ds;
    }

    private Item[] pickRandomItems(Item[] allItems, double percentOfItems, int class_case) {
        if (percentOfItems < 0.0 || percentOfItems > 1.0) {
            throw new IllegalArgumentException("Value for 'percentOfAllItems' argument should be between 0 and 1.");
        }
        HashMap<Integer, Item> pickedItems = new HashMap<Integer, Item>();
        block6 : for (Item item : allItems) {
            String item_name = item.getName();
            if (pickedItems.containsKey(item.getId())) continue;
            switch (class_case) {
                case 1: {
                    if (item_name.toLowerCase().charAt(5) > 'b') continue block6;
                    pickedItems.put(item.getId(), item);
                    continue block6;
                }
                case 2: {
                    if (item_name.toLowerCase().charAt(5) <= 'b' || item_name.toLowerCase().charAt(5) > 'd') continue block6;
                    pickedItems.put(item.getId(), item);
                    continue block6;
                }
                case 3: {
                    if (item_name.toLowerCase().charAt(5) <= 'd' || item_name.toLowerCase().charAt(5) > 'h') continue block6;
                    pickedItems.put(item.getId(), item);
                    continue block6;
                }
                case 4: {
                    if (item_name.toLowerCase().charAt(5) <= 'h' || item_name.toLowerCase().charAt(5) > 'z') continue block6;
                    pickedItems.put(item.getId(), item);
                }
            }
        }
        Random rand = new Random();
        int sampleSize = (int)Math.round(percentOfItems * (double)pickedItems.size());
        int counter = 0;
        while (counter < sampleSize && pickedItems.size() < this.numItems) {
            int itemId = rand.nextInt(this.numItems);
            Item item = allItems[itemId];
            if (pickedItems.containsKey(item.getId())) continue;
            pickedItems.put(item.getId(), item);
            ++counter;
        }
        return pickedItems.values().toArray(new Item[pickedItems.size()]);
    }

    private String[] generateUserLabels() {
        String[] users = new String[this.numUsers];
        int cloneF = this.numUsers / PARTIAL_IDS_LENGTH;
        for (int k = 0; k < PARTIAL_IDS_LENGTH; ++k) {
            for (int i = 0; i < cloneF; ++i) {
                users[i + k * cloneF] = new String(String.valueOf(PARTIAL_IDS[k]) + String.valueOf(i + 1) + "_user");
            }
        }
        return users;
    }

    private String[] generateItemLabels() {
        String[] items = new String[this.numItems];
        int cloneF = this.numItems / PARTIAL_IDS_LENGTH;
        for (int k = 0; k < PARTIAL_IDS_LENGTH; ++k) {
            for (int i = 0; i < cloneF; ++i) {
                items[i + k * cloneF] = new String("item_" + PARTIAL_IDS[k] + String.valueOf(i + 1));
            }
        }
        return items;
    }

    private Item[] loadAllItems() {
        String[] itemLabels = this.generateItemLabels();
        Item[] allItems = new Item[this.numItems];
        for (int i = 0; i < this.numItems; ++i) {
            Item item;
            int id = i;
            String name = itemLabels[i];
            allItems[i] = item = new Item((Integer)id, name);
        }
        return allItems;
    }
}

