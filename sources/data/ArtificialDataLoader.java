package bacon.data;

import bacon.data.DataLoader;
import iweb2.ch3.collaborative.data.BaseDataset;
import iweb2.ch3.collaborative.data.RatingBuilder;
import iweb2.ch3.collaborative.model.Dataset;
import iweb2.ch3.collaborative.model.Item;
import iweb2.ch3.collaborative.model.Rating;
import iweb2.ch3.collaborative.model.User;
import java.io.PrintStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class ArtificialDataLoader
implements DataLoader {
    public static final String[] PARTIAL_IDS = new String[]{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    public static int PARTIAL_IDS_LENGTH = PARTIAL_IDS.length;
    public static final int LOW_RATING = 1;
    public static final int HIGH_RATING = 5;
    public static int TWO_GROUPS = 2;
    public static int THREE_GROUPS = 3;
    public static double VOTES_PERCENTAGE_DEFAULT = 0.8;
    public static int MAX_SCALE_FACTOR = 1000;
    public int numUsers;
    public int numItems;
    public int numGroups;
    public int numRatings;
    public Dataset dataset;

    public ArtificialDataLoader(int userCloneFactor, int itemCloneFactor, int groups) {
        if (!this.checkParameters(userCloneFactor, itemCloneFactor, groups, VOTES_PERCENTAGE_DEFAULT)) {
            throw new RuntimeException("ERR - Cannot build artificial dataset. Unacceptable parameters.");
        }
        this.numUsers = userCloneFactor * PARTIAL_IDS_LENGTH;
        this.numItems = itemCloneFactor * PARTIAL_IDS_LENGTH;
        this.numGroups = groups;
        this.dataset = this.createDataset();
        this.numRatings = this.dataset.getRatingsCount();
        System.out.println("\nArtificial_1 dataset properties:");
        System.out.println("# users = " + this.numUsers);
        System.out.println("# items = " + this.numItems);
        System.out.println("# ratings = " + this.numRatings);
        System.out.println("# groups = " + this.numGroups);
        System.out.println("% of rated item per user = " + VOTES_PERCENTAGE_DEFAULT);
    }

    public ArtificialDataLoader(int userCloneFactor, int itemCloneFactor, int groups, double votePercentage) {
        if (!this.checkParameters(userCloneFactor, itemCloneFactor, groups, VOTES_PERCENTAGE_DEFAULT)) {
            throw new RuntimeException("ERR - Cannot build artificial dataset. Unacceptable parameters.");
        }
        this.numUsers = userCloneFactor * PARTIAL_IDS_LENGTH;
        this.numItems = itemCloneFactor * PARTIAL_IDS_LENGTH;
        this.numGroups = groups;
        this.dataset = this.createDataset(votePercentage);
        this.numRatings = this.dataset.getRatingsCount();
        System.out.println("\nArtificial_1 dataset properties:");
        System.out.println("# users = " + this.numUsers);
        System.out.println("# items = " + this.numItems);
        System.out.println("# ratings = " + this.numRatings);
        System.out.println("# groups = " + this.numGroups);
        System.out.println("% of rated item per user = " + votePercentage);
    }

    private boolean checkParameters(int u, int i, int g, double p) {
        return u > 0 && u <= MAX_SCALE_FACTOR && i > 0 && i < MAX_SCALE_FACTOR && (g == THREE_GROUPS || g == TWO_GROUPS) && p > 0.0 && p <= 1.0;
    }

    private String[] generateUserLabels() {
        String[] users = new String[this.numUsers];
        int cloneF = this.numUsers / PARTIAL_IDS_LENGTH;
        for (int k = 0; k < PARTIAL_IDS_LENGTH; ++k) {
            for (int i = 0; i < cloneF; ++i) {
                users[i + k * cloneF] = new String(String.valueOf(PARTIAL_IDS[k]) + String.valueOf(i + 1) + "_user");
            }
        }
        return users;
    }

    private String[] generateItemLabels() {
        String[] items = new String[this.numItems];
        int cloneF = this.numItems / PARTIAL_IDS_LENGTH;
        for (int k = 0; k < PARTIAL_IDS_LENGTH; ++k) {
            for (int i = 0; i < cloneF; ++i) {
                items[i + k * cloneF] = new String("item_" + PARTIAL_IDS[k] + String.valueOf(i + 1));
            }
        }
        return items;
    }

    private Item[] loadAllItems() {
        String[] itemLabels = this.generateItemLabels();
        Item[] allItems = new Item[this.numItems];
        for (int i = 0; i < this.numItems; ++i) {
            Item item;
            int id = i;
            String name = itemLabels[i];
            allItems[i] = item = new Item((Integer)id, name);
        }
        return allItems;
    }

    private Item[] pickRandomItems(Item[] allItems, double percentOfAllItems) {
        if (percentOfAllItems < 0.0 || percentOfAllItems > 1.0) {
            throw new IllegalArgumentException("Value for 'percentOfAllItems' argument should be between 0 and 1.");
        }
        Random rand = new Random();
        int sampleSize = (int)Math.round(percentOfAllItems * (double)this.numItems);
        HashMap<Integer, Item> pickedItems = new HashMap<Integer, Item>();
        while (pickedItems.size() < sampleSize) {
            int itemId = rand.nextInt(this.numItems);
            Item item = allItems[itemId];
            if (pickedItems.containsKey(item.getId())) continue;
            pickedItems.put(item.getId(), item);
        }
        return pickedItems.values().toArray(new Item[pickedItems.size()]);
    }

    private Dataset createDataset() {
        return this.createDataset(VOTES_PERCENTAGE_DEFAULT);
    }

    private Dataset createDataset(double percentOfItem) {
        BaseDataset ds = new BaseDataset();
        Item[] allItems = this.loadAllItems();
        String[] userLabels = this.generateUserLabels();
        for (int i = 0; i < this.numUsers; ++i) {
            int userId = i;
            String userName = userLabels[i];
            int lowRating = 1;
            int highRating = 5;
            switch (this.numGroups) {
                case 2: {
                    if (userName.toLowerCase().charAt(0) <= 'k') {
                        lowRating = 4;
                        highRating = 5;
                        break;
                    }
                    lowRating = 1;
                    highRating = 3;
                    break;
                }
                case 3: {
                    if (userName.toLowerCase().charAt(0) <= 'c') {
                        lowRating = 4;
                        highRating = 5;
                        break;
                    }
                    if (userName.toLowerCase().charAt(0) >= 'd' && userName.toLowerCase().charAt(0) <= 'i') {
                        lowRating = 3;
                        highRating = 4;
                        break;
                    }
                    lowRating = 1;
                    highRating = 2;
                }
            }
            Item[] items = this.pickRandomItems(allItems, percentOfItem);
            RatingBuilder ratingBuider = new RatingBuilder();
            List<Rating> ratings = ratingBuider.createBiasedRatings(userId, items, lowRating, highRating);
            User bu = new User(userId, userName, ratings);
            ds.add(bu);
        }
        return ds;
    }

    public static void main(String[] args) {
        ArtificialDataLoader ad = new ArtificialDataLoader(1, 1, 2);
        Dataset bs = ad.createDataset(0.8);
        ((BaseDataset)bs).printUserRatings();
    }

    @Override
    public Dataset loadDataset() {
        return this.dataset;
    }
}

