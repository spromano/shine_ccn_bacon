package bacon.data;

import bacon.data.DataLoader;
import bacon.utils.Configurator;
import iweb2.ch3.collaborative.model.Dataset;

public class DBDataLoader
implements DataLoader {
    public String DBname;

    public DBDataLoader(Configurator conf) {
        this.DBname = conf.getDBName();
    }

    public DBDataLoader(String dbname) {
        this.DBname = dbname;
    }

    public Dataset buildDatasetFromDB() {
        throw new RuntimeException("DB data loader not implemented.");
    }

    @Override
    public Dataset loadDataset() {
        return this.buildDatasetFromDB();
    }
}

