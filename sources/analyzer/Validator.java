package bacon.analyzer;

import bacon.analyzer.BaconAnalyzerImpl;
import bacon.clustering.DBSCANAlgorithm;
import iweb2.ch3.collaborative.model.Dataset;
import iweb2.ch3.collaborative.model.Item;
import iweb2.ch3.collaborative.model.User;
import iweb2.ch3.collaborative.recommender.Recommender;
import iweb2.ch4.model.Cluster;
import iweb2.ch4.model.DataPoint;
import java.io.PrintStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Validator {
    BaconAnalyzerImpl analyzer;

    public Validator(BaconAnalyzerImpl baconAnalyzer) {
        this.analyzer = baconAnalyzer;
    }

    public double getNoisePercentage() {
        double results = 0.0;
        List<Cluster> communities = this.analyzer.getCommunities();
        for (Cluster community : communities) {
            if (!String.valueOf("-1").equals(community.getLabel())) continue;
            results = (double)community.size() / (double)this.analyzer.getDataset().getUserCount() * 100.0;
        }
        return results;
    }

    public void printNoisePercentage() {
        System.out.println("\nNoise percentage = " + this.getNoisePercentage() + "%");
    }

    public double getPercentualAccuracy() {
        List<Cluster> communities = this.analyzer.getCommunities();
        double accuracy = 0.0;
        for (Cluster c : communities) {
            accuracy += this.getPercentualAccuracyForCommunity(c, false);
        }
        return accuracy / (double)communities.size() * 100.0;
    }

    public void printPercentualAccuracy() {
        System.out.println("\nAccuracy = " + this.getPercentualAccuracy() + "%");
    }

    public double getPercentualAccuracyWithoutNoise() {
        boolean isThereNoise = this.isThereNoise();
        if (!isThereNoise) {
            return this.getPercentualAccuracy();
        }
        List<Cluster> communities = this.analyzer.getCommunities();
        double community_accuracy = 0.0;
        for (Cluster community : communities) {
            if (String.valueOf(DBSCANAlgorithm.CLUSTER_ID_NOISE).equals(community.getLabel())) continue;
            boolean excludeNoise = true;
            community_accuracy += this.getPercentualAccuracyForCommunity(community, excludeNoise);
        }
        return community_accuracy / ((double)communities.size() - 1.0) * 100.0;
    }

    public void printPercentualAccuracyWithoutNoise() {
        System.out.println("\nAccuracy out of noise = " + this.getPercentualAccuracyWithoutNoise() + "%");
    }

    private double getPercentualAccuracyForCommunity(Cluster c, boolean excludeNoise) {
        Map<Integer, User> typical_users = this.analyzer.getTypicalUsers();
        User typical_user = typical_users.get(Integer.parseInt(c.getLabel()));
        double win_user = 0.0;
        for (DataPoint point : c.getElements()) {
            String[] tokens = point.getLabel().split("_");
            String user_id = tokens[tokens.length - 1];
            User user = this.analyzer.getDataset().getUser(Integer.parseInt(user_id));
            win_user += this.getPercentualAccuracyForUser(user, typical_user, excludeNoise);
        }
        return win_user / (double)c.size();
    }

    private double getPercentualAccuracyForUser(User user, User typical_user, boolean excludeNoise) {
        Collection<Item> items = this.analyzer.getDataset().getItems();
        double win_item = 0.0;
        for (Item i : items) {
            win_item += this.getWinPercentageForItem(user, typical_user, i, excludeNoise);
        }
        return win_item / (double)items.size();
    }

    private double getWinPercentageForItem(User user, User typical_user, Item item, boolean excludeNoise) {
        Map<Integer, User> typical_users = this.analyzer.getTypicalUsers();
        User noise_centroid = typical_users.get(DBSCANAlgorithm.CLUSTER_ID_NOISE);
        double user_rec = this.analyzer.getRecommender().predictRating(user, item);
        double typical_user_rec = this.analyzer.getRecommender().predictRating(typical_user, item);
        double same_cluster_diff = Math.abs(typical_user_rec - user_rec);
        int win = 0;
        int clus = 0;
        for (User other_centroid : typical_users.values()) {
            double other_centroid_rec;
            double other_cluster_diff;
            if (excludeNoise) {
                if (other_centroid.getId() == typical_user.getId() || other_centroid.getId() == noise_centroid.getId()) continue;
                ++clus;
                other_centroid_rec = this.analyzer.getRecommender().predictRating(other_centroid, item);
                other_cluster_diff = Math.abs(other_centroid_rec - user_rec);
                if (other_cluster_diff < same_cluster_diff) continue;
                ++win;
                continue;
            }
            if (other_centroid.getId() == typical_user.getId()) continue;
            ++clus;
            other_centroid_rec = this.analyzer.getRecommender().predictRating(other_centroid, item);
            other_cluster_diff = Math.abs(other_centroid_rec - user_rec);
            if (other_cluster_diff < same_cluster_diff) continue;
            ++win;
        }
        if (clus == 0) {
            return 1.0;
        }
        return (double)win / (double)clus;
    }

    public boolean isThereNoise() {
        boolean results = false;
        List<Cluster> communities = this.analyzer.getCommunities();
        for (Cluster community : communities) {
            if (!String.valueOf("-1").equals(community.getLabel())) continue;
            return true;
        }
        return results;
    }
}

