package bacon.analyzer;

import bacon.clustering.ClusteringPointsetBuilder;
import bacon.clustering.DBSCANCommunityDetector;
import bacon.model.BaconAnalyzer;
import bacon.model.CommunityDetector;
import iweb2.ch3.collaborative.model.Dataset;
import iweb2.ch3.collaborative.model.Item;
import iweb2.ch3.collaborative.model.User;
import iweb2.ch3.collaborative.recommender.Delphi;
import iweb2.ch3.collaborative.recommender.Recommender;
import iweb2.ch3.collaborative.similarity.RecommendationType;
import iweb2.ch3.collaborative.similarity.naive.SimilarityMatrix;
import iweb2.ch4.model.Cluster;
import iweb2.ch4.model.DataPoint;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class BaconAnalyzerImpl
implements BaconAnalyzer {
    private CommunityDetector detector;
    private Recommender recommender;
    private Dataset dataset;
    private DataPoint[] points;
    private double[][] dist_matrix;
    private List<Cluster> communities = null;
    private Map<Integer, User> typical_users = null;

    public BaconAnalyzerImpl(Dataset data) {
        this.init(data);
    }

    public BaconAnalyzerImpl(Dataset data, double epsilon, int minpoints) {
        this.init(data, epsilon, minpoints);
    }

    private void init(Dataset data) {
        this.dataset = data;
        SimilarityMatrix sim = ClusteringPointsetBuilder.buildBaconSimilarityMatrix(this.dataset);
        this.points = ClusteringPointsetBuilder.createDatapoints(this.dataset, sim);
        this.dist_matrix = ClusteringPointsetBuilder.buildDistanceMatrix(sim);
        this.detector = new DBSCANCommunityDetector(this.points, this.dist_matrix);
        this.recommender = new Delphi(this.dataset, RecommendationType.USER_BASED, sim);
    }

    private void init(Dataset data, double epsilon, int minpoints) {
        this.dataset = data;
        SimilarityMatrix sim = ClusteringPointsetBuilder.buildBaconSimilarityMatrix(this.dataset);
        this.points = ClusteringPointsetBuilder.createDatapoints(this.dataset, sim);
        this.dist_matrix = ClusteringPointsetBuilder.buildDistanceMatrix(sim);
        this.detector = new DBSCANCommunityDetector(this.points, this.dist_matrix, epsilon, minpoints);
        this.recommender = new Delphi(this.dataset, RecommendationType.USER_BASED, sim);
    }

    @Override
    public void setDataset(Dataset dataset) {
        this.dataset = dataset;
    }

    @Override
    public void setCommunityDetector(CommunityDetector detector) {
        this.detector = detector;
    }

    @Override
    public void setRecommender(Recommender recommender) {
        this.recommender = recommender;
    }

    @Override
    public List<Cluster> detectCommunities() {
        this.detector.run();
        if (this.detector.isDone()) {
            this.communities = this.detector.getCommunities();
            return this.communities;
        }
        return null;
    }

    @Override
    public List<Cluster> getCommunities() {
        return this.communities;
    }

    @Override
    public void printCommunityDetectionResults() {
        this.detector.printResults();
    }

    @Override
    public void printCommunityDetectionSummary() {
        this.detector.printDetectionSummary();
    }

    @Override
    public Map<Integer, User> detectTypicalUsers() {
        if (this.detector.isDone()) {
            Map<Integer, DataPoint> typicalDataPoints = this.detector.getTypicalDataPoints();
            this.typical_users = new LinkedHashMap<Integer, User>();
            Iterator<Integer> iterator = typicalDataPoints.keySet().iterator();
            while (iterator.hasNext()) {
                int i = iterator.next();
                DataPoint centroid_point = typicalDataPoints.get(i);
                String[] tokens = centroid_point.getLabel().split("_");
                String centroid_user_id = tokens[tokens.length - 1];
                User centroid_user = this.dataset.getUser(Integer.parseInt(centroid_user_id));
                this.typical_users.put(i, centroid_user);
            }
            return this.typical_users;
        }
        return null;
    }

    @Override
    public Map<Integer, User> getTypicalUsers() {
        return this.typical_users;
    }

    @Override
    public void printTypicalUsers() {
        System.out.println();
        for (Integer i : this.typical_users.keySet()) {
            System.out.println("Typical user of community " + i + ": " + "name = " + this.typical_users.get(i).getName() + ", id = " + this.typical_users.get(i).getId());
        }
    }

    @Override
    public double getContentEvaluationForCommunity(int community_id, Item item) {
        return this.recommender.predictRating(this.typical_users.get(community_id), item);
    }

    @Override
    public Map<Integer, Double> getContentEvaluationVector(Item item) {
        LinkedHashMap<Integer, Double> estimatedRatings = new LinkedHashMap<Integer, Double>();
        Double rating = null;
        for (Integer k : this.typical_users.keySet()) {
            rating = this.getContentEvaluationForCommunity(k, item);
            estimatedRatings.put(k, rating);
        }
        return estimatedRatings;
    }

    @Override
    public void printContentEvaluationVector(Item item) {
        Map<Integer, Double> evaluation_vector = this.getContentEvaluationVector(item);
        System.out.println("\nContent '" + item.getName() + "' utilities:");
        for (Integer i : evaluation_vector.keySet()) {
            System.out.println("Community " + i + ") " + evaluation_vector.get(i));
        }
    }

    @Override
    public double getContentUtility(Item item) {
        Map<Integer, Double> utility_vector = this.getContentEvaluationVector(item);
        double utility = 0.0;
        for (Cluster c : this.communities) {
            utility += (double)c.size() * utility_vector.get(Integer.parseInt(c.getLabel()));
        }
        return utility / (double)this.dataset.getUserCount();
    }

    @Override
    public void printContentUtility(Item item) {
        System.out.println("\nContent '" + item.getName() + "' utility: " + this.getContentUtility(item));
    }

    public CommunityDetector getCommunityDetector() {
        return this.detector;
    }

    public Recommender getRecommender() {
        return this.recommender;
    }

    protected Dataset getDataset() {
        return this.dataset;
    }

    public double[][] getDistanceMatrix() {
        return this.detector.getDistanceMatrix();
    }
}

