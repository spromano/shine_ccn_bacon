package bacon.gui;

import bacon.gui.BaconMenu;
import bacon.gui.CommunityDetectionMenu;
import bacon.gui.TuningMenu;
import iweb2.ch3.collaborative.data.MovieLensData;
import iweb2.ch3.collaborative.data.MovieLensDataset;
import iweb2.ch3.collaborative.model.Dataset;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class MovielensSettings
extends JFrame {
    private JPanel contentPane;
    private JTextField textField;
    private JLabel error_label;
    private String test;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable(){

            @Override
            public void run() {
                try {
                    String test = BaconMenu.TEST_OPTIONS[BaconMenu.COMM_DET_INDEX];
                    MovielensSettings frame = new MovielensSettings(test);
                    frame.setVisible(true);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public MovielensSettings(String test) {
        this.test = test;
        this.setTitle("MovieLens Settings");
        this.setDefaultCloseOperation(3);
        this.setBounds(100, 100, 450, 300);
        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setContentPane(this.contentPane);
        GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[3];
        gbl_contentPane.rowHeights = new int[6];
        gbl_contentPane.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
        gbl_contentPane.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        this.contentPane.setLayout(gbl_contentPane);
        JLabel lblDatasetDescription = new JLabel("Dataset description:");
        GridBagConstraints gbc_lblDatasetDescription = new GridBagConstraints();
        gbc_lblDatasetDescription.anchor = 17;
        gbc_lblDatasetDescription.insets = new Insets(20, 20, 5, 5);
        gbc_lblDatasetDescription.gridx = 0;
        gbc_lblDatasetDescription.gridy = 0;
        this.contentPane.add((Component)lblDatasetDescription, gbc_lblDatasetDescription);
        String description = "The MovieLens dataset consists of 100,000 ratings by 943 users\n for 1,682 movies.\nPlease provide the path to the MovieLens dataset folder.";
        JTextArea textArea = new JTextArea(description);
        GridBagConstraints gbc_textArea = new GridBagConstraints();
        gbc_textArea.gridwidth = 2;
        gbc_textArea.insets = new Insets(5, 20, 20, 20);
        gbc_textArea.fill = 1;
        gbc_textArea.gridx = 0;
        gbc_textArea.gridy = 1;
        this.contentPane.add((Component)textArea, gbc_textArea);
        JLabel lblMovielensFolderPath = new JLabel("Movielens folder path:");
        GridBagConstraints gbc_lblMovielensFolderPath = new GridBagConstraints();
        gbc_lblMovielensFolderPath.insets = new Insets(0, 0, 5, 5);
        gbc_lblMovielensFolderPath.anchor = 13;
        gbc_lblMovielensFolderPath.gridx = 0;
        gbc_lblMovielensFolderPath.gridy = 2;
        this.contentPane.add((Component)lblMovielensFolderPath, gbc_lblMovielensFolderPath);
        this.textField = new JTextField();
        GridBagConstraints gbc_textField = new GridBagConstraints();
        gbc_textField.insets = new Insets(5, 5, 5, 20);
        gbc_textField.fill = 2;
        gbc_textField.gridx = 1;
        gbc_textField.gridy = 2;
        this.contentPane.add((Component)this.textField, gbc_textField);
        this.textField.setColumns(10);
        this.error_label = new JLabel("");
        GridBagConstraints gbc_label = new GridBagConstraints();
        gbc_label.insets = new Insets(0, 0, 5, 0);
        gbc_label.gridx = 1;
        gbc_label.gridy = 3;
        this.contentPane.add((Component)this.error_label, gbc_label);
        JButton btnSubmit = new JButton("Submit");
        btnSubmit.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                MovielensSettings.this.create_dataset_and_launch_test();
            }
        });
        GridBagConstraints gbc_btnSubmit = new GridBagConstraints();
        gbc_btnSubmit.insets = new Insets(5, 5, 20, 20);
        gbc_btnSubmit.anchor = 13;
        gbc_btnSubmit.gridx = 1;
        gbc_btnSubmit.gridy = 4;
        this.contentPane.add((Component)btnSubmit, gbc_btnSubmit);
    }

    private void create_dataset_and_launch_test() {
        if (this.textField.getText().equals("")) {
            this.error_label.setText("Please fill the path field.");
        } else {
            String path = this.textField.getText();
            try {
                FileInputStream fis = new FileInputStream(String.valueOf(path) + "ratings.dat");
            }
            catch (FileNotFoundException e) {
                this.error_label.setText("Invalid path. Please provide a new one.");
            }
            MovieLensDataset movielens = MovieLensData.createDataset(path, 0);
            if (this.test.equals(BaconMenu.TEST_OPTIONS[BaconMenu.COMM_DET_INDEX])) {
                CommunityDetectionMenu comm_det_menu = new CommunityDetectionMenu(movielens);
                comm_det_menu.setVisible(true);
                this.dispose();
            } else if (this.test.equals(BaconMenu.TEST_OPTIONS[BaconMenu.TUNING_INDEX])) {
                TuningMenu tuning_menu = new TuningMenu(movielens);
                tuning_menu.setVisible(true);
                this.dispose();
            }
        }
    }

}

