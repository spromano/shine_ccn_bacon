package bacon.gui;

import bacon.gui.ResultsAndContentEvaluationWindow;
import iweb2.ch3.collaborative.data.BaseDataset;
import iweb2.ch3.collaborative.data.MusicData;
import iweb2.ch3.collaborative.model.Dataset;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class CommunityDetectionMenu
extends JFrame {
    private JPanel contentPane;
    private JTextField epsilon_textField;
    private JTextField minpoints_textField;
    private JLabel errorLabel;
    private JCheckBox rdbtnPrintClustersProperties;
    private JCheckBox rdbtnPrintValidationResults;
    private Dataset dataset;
    private JLabel lblBest;
    private JTextArea textArea;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable(){

            @Override
            public void run() {
                try {
                    BaseDataset dataset = MusicData.createDataset();
                    CommunityDetectionMenu frame = new CommunityDetectionMenu(dataset);
                    frame.setVisible(true);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public CommunityDetectionMenu(Dataset dataset) {
        this.dataset = dataset;
        this.setTitle("Community Detection Settings");
        this.setDefaultCloseOperation(3);
        this.setBounds(100, 100, 462, 415);
        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setContentPane(this.contentPane);
        GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[4];
        gbl_contentPane.rowHeights = new int[14];
        gbl_contentPane.columnWeights = new double[]{1.0, 0.0, 1.0, Double.MIN_VALUE};
        gbl_contentPane.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        this.contentPane.setLayout(gbl_contentPane);
        this.lblBest = new JLabel("Recommended settings");
        GridBagConstraints gbc_lblBest = new GridBagConstraints();
        gbc_lblBest.anchor = 17;
        gbc_lblBest.insets = new Insets(5, 20, 5, 5);
        gbc_lblBest.gridx = 0;
        gbc_lblBest.gridy = 0;
        this.contentPane.add((Component)this.lblBest, gbc_lblBest);
        String best_settings = "- music data: epsilon = 0.8, minpoints = 2\n- artificial_1 data: epsilon = 0.8, minpoints = 2\n- artificial_2 data: epsilon = 0.5, minpoints = 2\n- movielens data: epsilon = 0.87, minpoints = 4\n\nPlease note that cluster properties and validation can significantly \nimprove computation time.";
        this.textArea = new JTextArea(best_settings);
        this.textArea.setEditable(false);
        GridBagConstraints gbc_textArea = new GridBagConstraints();
        gbc_textArea.gridwidth = 3;
        gbc_textArea.insets = new Insets(5, 20, 5, 20);
        gbc_textArea.fill = 1;
        gbc_textArea.gridx = 0;
        gbc_textArea.gridy = 1;
        this.contentPane.add((Component)this.textArea, gbc_textArea);
        JLabel lblDbscanEpsilon = new JLabel("Set DBSCAN epsilon:");
        GridBagConstraints gbc_lblDbscanEpsilon = new GridBagConstraints();
        gbc_lblDbscanEpsilon.insets = new Insets(20, 20, 5, 5);
        gbc_lblDbscanEpsilon.anchor = 13;
        gbc_lblDbscanEpsilon.gridx = 0;
        gbc_lblDbscanEpsilon.gridy = 2;
        this.contentPane.add((Component)lblDbscanEpsilon, gbc_lblDbscanEpsilon);
        this.epsilon_textField = new JTextField();
        GridBagConstraints gbc_epsilon_textField = new GridBagConstraints();
        gbc_epsilon_textField.insets = new Insets(20, 5, 5, 20);
        gbc_epsilon_textField.fill = 2;
        gbc_epsilon_textField.gridx = 2;
        gbc_epsilon_textField.gridy = 2;
        this.contentPane.add((Component)this.epsilon_textField, gbc_epsilon_textField);
        this.epsilon_textField.setColumns(10);
        JLabel lblSetDbscanMinpoints = new JLabel("Set DBSCAN minpoints:");
        GridBagConstraints gbc_lblSetDbscanMinpoints = new GridBagConstraints();
        gbc_lblSetDbscanMinpoints.anchor = 13;
        gbc_lblSetDbscanMinpoints.insets = new Insets(5, 20, 5, 5);
        gbc_lblSetDbscanMinpoints.gridx = 0;
        gbc_lblSetDbscanMinpoints.gridy = 3;
        this.contentPane.add((Component)lblSetDbscanMinpoints, gbc_lblSetDbscanMinpoints);
        this.minpoints_textField = new JTextField();
        GridBagConstraints gbc_minpoints_textField = new GridBagConstraints();
        gbc_minpoints_textField.insets = new Insets(5, 5, 5, 20);
        gbc_minpoints_textField.fill = 2;
        gbc_minpoints_textField.gridx = 2;
        gbc_minpoints_textField.gridy = 3;
        this.contentPane.add((Component)this.minpoints_textField, gbc_minpoints_textField);
        this.minpoints_textField.setColumns(10);
        this.rdbtnPrintClustersProperties = new JCheckBox("Print clusters properties");
        GridBagConstraints gbc_rdbtnPrintClustersProperties = new GridBagConstraints();
        gbc_rdbtnPrintClustersProperties.anchor = 17;
        gbc_rdbtnPrintClustersProperties.insets = new Insets(5, 5, 5, 5);
        gbc_rdbtnPrintClustersProperties.gridx = 2;
        gbc_rdbtnPrintClustersProperties.gridy = 4;
        this.contentPane.add((Component)this.rdbtnPrintClustersProperties, gbc_rdbtnPrintClustersProperties);
        this.rdbtnPrintValidationResults = new JCheckBox("Print validation results");
        GridBagConstraints gbc_rdbtnPrintValidationResults = new GridBagConstraints();
        gbc_rdbtnPrintValidationResults.anchor = 17;
        gbc_rdbtnPrintValidationResults.insets = new Insets(5, 5, 5, 5);
        gbc_rdbtnPrintValidationResults.gridx = 2;
        gbc_rdbtnPrintValidationResults.gridy = 5;
        this.contentPane.add((Component)this.rdbtnPrintValidationResults, gbc_rdbtnPrintValidationResults);
        JButton btnStartCommunityDetection = new JButton("Continue");
        GridBagConstraints gbc_btnStartCommunityDetection = new GridBagConstraints();
        gbc_btnStartCommunityDetection.anchor = 13;
        gbc_btnStartCommunityDetection.insets = new Insets(20, 0, 5, 20);
        gbc_btnStartCommunityDetection.gridx = 2;
        gbc_btnStartCommunityDetection.gridy = 7;
        btnStartCommunityDetection.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                CommunityDetectionMenu.this.start_community_detection();
            }
        });
        this.errorLabel = new JLabel("");
        GridBagConstraints gbc_errorLabel = new GridBagConstraints();
        gbc_errorLabel.anchor = 13;
        gbc_errorLabel.insets = new Insets(5, 5, 5, 20);
        gbc_errorLabel.gridx = 2;
        gbc_errorLabel.gridy = 6;
        this.contentPane.add((Component)this.errorLabel, gbc_errorLabel);
        this.contentPane.add((Component)btnStartCommunityDetection, gbc_btnStartCommunityDetection);
    }

    private void start_community_detection() {
        if (this.epsilon_textField.getText().equals("") || this.minpoints_textField.getText().equals("")) {
            this.errorLabel.setText("Please fill all the fields.");
        } else {
            boolean check;
            double epsilon = Double.parseDouble(this.epsilon_textField.getText());
            int minpoints = Integer.parseInt(this.minpoints_textField.getText());
            boolean bl = check = epsilon > 0.0 && epsilon < 1.0 && minpoints > 1 && minpoints < 10;
            if (check) {
                boolean cluster_props = this.rdbtnPrintClustersProperties.isSelected();
                boolean validation = this.rdbtnPrintValidationResults.isSelected();
                ResultsAndContentEvaluationWindow results = new ResultsAndContentEvaluationWindow(this.dataset, epsilon, minpoints, cluster_props, validation);
                results.setVisible(true);
                this.dispose();
            } else {
                this.errorLabel.setText("Unacceptable parameters. Please provide new values.");
            }
        }
    }

}

