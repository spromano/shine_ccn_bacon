package bacon.gui;

import bacon.data.ArtificialDataLoader;
import bacon.gui.BaconMenu;
import bacon.gui.CommunityDetectionMenu;
import bacon.gui.TuningMenu;
import iweb2.ch3.collaborative.model.Dataset;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class Artificial1ConfMenu
extends JFrame {
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField num_users_textField;
    private JTextField num_items_textField;
    private JTextField num_groups_textField;
    private JTextField votes_perc_textField;
    private String test;
    private JLabel errorLabel;
    private JLabel lblNewLabel;
    private JTextArea textArea;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable(){

            @Override
            public void run() {
                try {
                    String test = BaconMenu.TEST_OPTIONS[BaconMenu.COMM_DET_INDEX];
                    Artificial1ConfMenu frame = new Artificial1ConfMenu(test);
                    frame.setVisible(true);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public Artificial1ConfMenu(String test) {
        this.test = test;
        this.setTitle("Artificial_1 Dataset Settings");
        this.setDefaultCloseOperation(3);
        this.setBounds(100, 100, 505, 548);
        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setContentPane(this.contentPane);
        GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[4];
        gbl_contentPane.rowHeights = new int[10];
        gbl_contentPane.columnWeights = new double[]{1.0, 0.0, 1.0, Double.MIN_VALUE};
        gbl_contentPane.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        this.contentPane.setLayout(gbl_contentPane);
        this.lblNewLabel = new JLabel("Dataset description:");
        GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
        gbc_lblNewLabel.anchor = 17;
        gbc_lblNewLabel.insets = new Insets(20, 20, 5, 5);
        gbc_lblNewLabel.gridx = 0;
        gbc_lblNewLabel.gridy = 0;
        this.contentPane.add((Component)this.lblNewLabel, gbc_lblNewLabel);
        String description = "This dataset has two possible configurations:\n- two groups: \n-- (A-I) users with ratings (4-5), \n-- (L-Z) users with ratings (1-3);\n- three groups: \n-- (A-C) users with ratings (4-5)\n-- (D-I) users with ratings (3-4)\n-- (L-Z) users with ratings (1-2)\n\n You can configure:\n- the number of groups (2-3)\n- the users scale factor U (you will create U*26 users)\n- the items scale factor I (you will create I*26 items)\n- the percentage of items each user can vote for.";
        this.textArea = new JTextArea(description);
        this.textArea.setEditable(false);
        GridBagConstraints gbc_textArea = new GridBagConstraints();
        gbc_textArea.gridwidth = 3;
        gbc_textArea.insets = new Insets(5, 20, 5, 20);
        gbc_textArea.fill = 1;
        gbc_textArea.gridx = 0;
        gbc_textArea.gridy = 1;
        this.contentPane.add((Component)this.textArea, gbc_textArea);
        JLabel lblNumUsers = new JLabel("users scale factor");
        GridBagConstraints gbc_lblNumUsers = new GridBagConstraints();
        gbc_lblNumUsers.anchor = 13;
        gbc_lblNumUsers.insets = new Insets(20, 5, 5, 5);
        gbc_lblNumUsers.gridx = 0;
        gbc_lblNumUsers.gridy = 2;
        this.contentPane.add((Component)lblNumUsers, gbc_lblNumUsers);
        this.num_users_textField = new JTextField();
        GridBagConstraints gbc_num_users_textField = new GridBagConstraints();
        gbc_num_users_textField.insets = new Insets(20, 5, 5, 20);
        gbc_num_users_textField.fill = 2;
        gbc_num_users_textField.gridx = 2;
        gbc_num_users_textField.gridy = 2;
        this.contentPane.add((Component)this.num_users_textField, gbc_num_users_textField);
        this.num_users_textField.setColumns(10);
        JLabel lblNumItems = new JLabel("items scale factor");
        GridBagConstraints gbc_lblNumItems = new GridBagConstraints();
        gbc_lblNumItems.anchor = 13;
        gbc_lblNumItems.insets = new Insets(5, 5, 5, 5);
        gbc_lblNumItems.gridx = 0;
        gbc_lblNumItems.gridy = 3;
        this.contentPane.add((Component)lblNumItems, gbc_lblNumItems);
        this.num_items_textField = new JTextField();
        GridBagConstraints gbc_num_items_textField = new GridBagConstraints();
        gbc_num_items_textField.insets = new Insets(5, 5, 5, 20);
        gbc_num_items_textField.fill = 2;
        gbc_num_items_textField.gridx = 2;
        gbc_num_items_textField.gridy = 3;
        this.contentPane.add((Component)this.num_items_textField, gbc_num_items_textField);
        this.num_items_textField.setColumns(10);
        JLabel lblNumGroups = new JLabel("num groups");
        GridBagConstraints gbc_lblNumGroups = new GridBagConstraints();
        gbc_lblNumGroups.anchor = 13;
        gbc_lblNumGroups.insets = new Insets(5, 5, 5, 5);
        gbc_lblNumGroups.gridx = 0;
        gbc_lblNumGroups.gridy = 4;
        this.contentPane.add((Component)lblNumGroups, gbc_lblNumGroups);
        this.num_groups_textField = new JTextField();
        GridBagConstraints gbc_num_groups_textField = new GridBagConstraints();
        gbc_num_groups_textField.insets = new Insets(5, 5, 5, 20);
        gbc_num_groups_textField.fill = 2;
        gbc_num_groups_textField.gridx = 2;
        gbc_num_groups_textField.gridy = 4;
        this.contentPane.add((Component)this.num_groups_textField, gbc_num_groups_textField);
        this.num_groups_textField.setColumns(10);
        JLabel lblOfVoted = new JLabel("% of voted items");
        GridBagConstraints gbc_lblOfVoted = new GridBagConstraints();
        gbc_lblOfVoted.anchor = 13;
        gbc_lblOfVoted.insets = new Insets(5, 5, 5, 5);
        gbc_lblOfVoted.gridx = 0;
        gbc_lblOfVoted.gridy = 5;
        this.contentPane.add((Component)lblOfVoted, gbc_lblOfVoted);
        this.votes_perc_textField = new JTextField();
        GridBagConstraints gbc_votes_perc_textField = new GridBagConstraints();
        gbc_votes_perc_textField.insets = new Insets(5, 5, 5, 20);
        gbc_votes_perc_textField.fill = 2;
        gbc_votes_perc_textField.gridx = 2;
        gbc_votes_perc_textField.gridy = 5;
        this.contentPane.add((Component)this.votes_perc_textField, gbc_votes_perc_textField);
        this.votes_perc_textField.setColumns(10);
        JButton btnContinue = new JButton("Continue");
        btnContinue.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                Artificial1ConfMenu.this.continue_action();
            }
        });
        this.errorLabel = new JLabel("");
        GridBagConstraints gbc_errorLabel = new GridBagConstraints();
        gbc_errorLabel.anchor = 17;
        gbc_errorLabel.insets = new Insets(5, 5, 5, 20);
        gbc_errorLabel.gridwidth = 3;
        gbc_errorLabel.gridx = 2;
        gbc_errorLabel.gridy = 6;
        this.contentPane.add((Component)this.errorLabel, gbc_errorLabel);
        GridBagConstraints gbc_btnContinue = new GridBagConstraints();
        gbc_btnContinue.insets = new Insets(5, 5, 20, 20);
        gbc_btnContinue.anchor = 13;
        gbc_btnContinue.gridx = 2;
        gbc_btnContinue.gridy = 7;
        this.contentPane.add((Component)btnContinue, gbc_btnContinue);
    }

    private void continue_action() {
        if (this.num_groups_textField.getText().equals("") || this.num_items_textField.getText().equals("") || this.num_users_textField.getText().equals("") || this.votes_perc_textField.getText().equals("")) {
            this.errorLabel.setText("Please fill all the fields.");
        } else {
            boolean check;
            int users = Integer.parseInt(this.num_users_textField.getText());
            int items = Integer.parseInt(this.num_items_textField.getText());
            int groups = Integer.parseInt(this.num_groups_textField.getText());
            double perc_votes = Double.parseDouble(this.votes_perc_textField.getText());
            boolean bl = check = users > 0 && users < 500 && items > 0 && items < 500 && (groups == 2 || groups == 3) && perc_votes > 0.0 && perc_votes <= 1.0;
            if (check) {
                ArtificialDataLoader loader = new ArtificialDataLoader(users, items, groups, perc_votes);
                Dataset dataset = loader.loadDataset();
                if (this.test.equals(BaconMenu.TEST_OPTIONS[0])) {
                    CommunityDetectionMenu comm_det_menu = new CommunityDetectionMenu(dataset);
                    comm_det_menu.setVisible(true);
                    this.dispose();
                } else if (this.test.equals(BaconMenu.TEST_OPTIONS[1])) {
                    TuningMenu tuning_menu = new TuningMenu(dataset);
                    tuning_menu.setVisible(true);
                    this.dispose();
                }
            } else {
                this.errorLabel.setText("Unacceptable parameters.");
            }
        }
    }

}

