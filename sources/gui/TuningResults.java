package bacon.gui;

import bacon.clustering.ClusteringPointsetBuilder;
import bacon.clustering.DBSCANAlgorithm;
import bacon.data.ArtificialDataLoader;
import bacon.gui.BaconMenu;
import bacon.utils.Chronometer;
import iweb2.ch3.collaborative.model.Dataset;
import iweb2.ch3.collaborative.similarity.naive.SimilarityMatrix;
import iweb2.ch4.model.Cluster;
import iweb2.ch4.model.DataPoint;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Set;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class TuningResults
extends JFrame {
    private JPanel contentPane;
    private double min_eps;
    private double max_eps;
    private double eps_step;
    private int min_points;
    private int max_points;
    private int minpoints_step;
    private boolean clus_props;
    private Dataset dataset;
    private JTextArea textArea;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable(){

            @Override
            public void run() {
                try {
                    ArtificialDataLoader loader = new ArtificialDataLoader(1, 1, 2, 1.0);
                    Dataset dataset = loader.loadDataset();
                    TuningResults tr = new TuningResults(dataset, 0.75, 0.95, 0.1, 2, 6, 1, true);
                    tr.setVisible(true);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public TuningResults(Dataset dataset, double min_eps, double max_eps, double eps_step, int min_points, int max_points, int minpoints_step, boolean clus_props) {
        this.dataset = dataset;
        this.min_eps = min_eps;
        this.max_eps = max_eps;
        this.eps_step = eps_step;
        this.min_points = min_points;
        this.max_points = max_points;
        this.minpoints_step = minpoints_step;
        this.clus_props = clus_props;
        this.setTitle("Tuning results");
        this.setDefaultCloseOperation(3);
        this.setBounds(100, 100, 631, 499);
        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setContentPane(this.contentPane);
        GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[9];
        gbl_contentPane.rowHeights = new int[7];
        gbl_contentPane.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
        gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
        this.contentPane.setLayout(gbl_contentPane);
        JButton btnStart = new JButton("Start");
        btnStart.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                TuningResults.this.start_tuning();
            }
        });
        GridBagConstraints gbc_btnStart = new GridBagConstraints();
        gbc_btnStart.gridwidth = 7;
        gbc_btnStart.insets = new Insets(20, 5, 20, 0);
        gbc_btnStart.gridx = 1;
        gbc_btnStart.gridy = 0;
        this.contentPane.add((Component)btnStart, gbc_btnStart);
        this.textArea = new JTextArea();
        JScrollPane pane = new JScrollPane(this.textArea);
        GridBagConstraints gbc_textArea = new GridBagConstraints();
        gbc_textArea.insets = new Insets(20, 20, 20, 20);
        gbc_textArea.gridwidth = 8;
        gbc_textArea.gridheight = 4;
        gbc_textArea.fill = 1;
        gbc_textArea.gridx = 0;
        gbc_textArea.gridy = 1;
        this.contentPane.add((Component)pane, gbc_textArea);
        JButton btnNewTest = new JButton("New Test");
        btnNewTest.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                TuningResults.this.new_menu();
            }
        });
        GridBagConstraints gbc_btnNewTest = new GridBagConstraints();
        gbc_btnNewTest.insets = new Insets(5, 5, 20, 20);
        gbc_btnNewTest.anchor = 13;
        gbc_btnNewTest.gridx = 7;
        gbc_btnNewTest.gridy = 5;
        this.contentPane.add((Component)btnNewTest, gbc_btnNewTest);
    }

    private void new_menu() {
        BaconMenu menu = new BaconMenu();
        menu.setVisible(true);
        this.dispose();
    }

    private void start_tuning() {
        Chronometer timer = new Chronometer();
        this.textArea.append("\nBuilding distance matrix...\n");
        timer.startChronometer();
        SimilarityMatrix sim = ClusteringPointsetBuilder.buildBaconSimilarityMatrix(this.dataset);
        double[][] distance_matrix = ClusteringPointsetBuilder.buildDistanceMatrix(sim);
        timer.stopChronometer();
        this.textArea.append("Matrix built in " + timer.getElapsedTime() + " s\n");
        DataPoint[] points = ClusteringPointsetBuilder.createDatapoints(this.dataset, sim);
        double STARTING_EPSILON = this.min_eps;
        double EPSILON_STEP = this.eps_step;
        double MAX_EPSILON = this.max_eps;
        int STARTING_MINPOINTS = this.min_points;
        int MINPOINTS_STEP = this.minpoints_step;
        int MAX_MINPOINTS = this.max_points;
        for (double epsilon = STARTING_EPSILON; epsilon < MAX_EPSILON; epsilon += EPSILON_STEP) {
            for (int minPoints = STARTING_MINPOINTS; minPoints < MAX_MINPOINTS; minPoints += MINPOINTS_STEP) {
                this.textArea.append("\nRunning DBSCAN...\n");
                timer.startChronometer();
                DBSCANAlgorithm dbscan = new DBSCANAlgorithm(points, distance_matrix, epsilon, minPoints);
                List<Cluster> clusters = dbscan.cluster();
                timer.stopChronometer();
                this.textArea.append("DBSCAN completed in " + timer.getElapsedTime() + " s\n\n");
                this.textArea.append("\nDBSCAN Clustering Summary\n");
                this.textArea.append("Parameters:\n epsilon = " + epsilon + ",  minPoints=" + minPoints + "\n");
                this.textArea.append("# clusters = " + clusters.size() + "\n");
                boolean noiseElements_ = false;
                int num_noise = 0;
                for (Cluster c : clusters) {
                    if (String.valueOf(DBSCANAlgorithm.CLUSTER_ID_NOISE).equals(c.getLabel())) {
                        noiseElements_ = true;
                        num_noise = c.getElements().size();
                        continue;
                    }
                    this.textArea.append("Cluster " + c.getLabel() + ", # points = " + c.getElements().size() + "\n");
                }
                if (noiseElements_) {
                    this.textArea.append("Noise Cluster, # points = " + num_noise + "\n");
                }
                this.textArea.append("\n");
            }
        }
    }

}

