package bacon.gui;

import bacon.gui.BaconMenu;
import bacon.gui.CommunityDetectionMenu;
import bacon.gui.TuningMenu;
import iweb2.ch3.collaborative.data.BaseDataset;
import iweb2.ch3.collaborative.data.MusicData;
import iweb2.ch3.collaborative.model.Dataset;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class MusicDataConfMenu
extends JFrame {
    private JPanel contentPane;
    private JTextField textField;
    private JLabel error_label;
    private String test;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable(){

            @Override
            public void run() {
                try {
                    String tuning = BaconMenu.TEST_OPTIONS[BaconMenu.TUNING_INDEX];
                    MusicDataConfMenu frame = new MusicDataConfMenu(tuning);
                    frame.setVisible(true);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public MusicDataConfMenu(String test) {
        this.test = test;
        this.setTitle("Music Dataset Settings");
        this.setDefaultCloseOperation(3);
        this.setBounds(100, 100, 517, 443);
        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setContentPane(this.contentPane);
        GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[10];
        gbl_contentPane.rowHeights = new int[6];
        gbl_contentPane.columnWeights = new double[]{1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
        gbl_contentPane.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
        this.contentPane.setLayout(gbl_contentPane);
        JLabel lblDatasetDescription = new JLabel("Dataset Description");
        GridBagConstraints gbc_lblDatasetDescription = new GridBagConstraints();
        gbc_lblDatasetDescription.anchor = 17;
        gbc_lblDatasetDescription.insets = new Insets(20, 20, 5, 20);
        gbc_lblDatasetDescription.gridx = 0;
        gbc_lblDatasetDescription.gridy = 0;
        this.contentPane.add((Component)lblDatasetDescription, gbc_lblDatasetDescription);
        String dataset_descr = "Dataset configured with two classes:\n- (A-D) with votes (4-5), \n- (E-Z) with votes (1-3).\nYou can configure the percentage of the available items voted by each user.";
        JTextArea textArea = new JTextArea(dataset_descr);
        textArea.setEditable(false);
        GridBagConstraints gbc_textArea = new GridBagConstraints();
        gbc_textArea.gridwidth = 9;
        gbc_textArea.insets = new Insets(20, 20, 20, 20);
        gbc_textArea.fill = 1;
        gbc_textArea.gridx = 0;
        gbc_textArea.gridy = 1;
        this.contentPane.add((Component)textArea, gbc_textArea);
        JLabel lblOfVoted = new JLabel("% of voted items");
        GridBagConstraints gbc_lblOfVoted = new GridBagConstraints();
        gbc_lblOfVoted.anchor = 13;
        gbc_lblOfVoted.insets = new Insets(20, 20, 20, 5);
        gbc_lblOfVoted.gridx = 0;
        gbc_lblOfVoted.gridy = 2;
        this.contentPane.add((Component)lblOfVoted, gbc_lblOfVoted);
        this.textField = new JTextField();
        GridBagConstraints gbc_textField = new GridBagConstraints();
        gbc_textField.gridwidth = 8;
        gbc_textField.insets = new Insets(20, 5, 20, 20);
        gbc_textField.fill = 2;
        gbc_textField.gridx = 1;
        gbc_textField.gridy = 2;
        this.contentPane.add((Component)this.textField, gbc_textField);
        this.textField.setColumns(10);
        JButton btnContinue = new JButton("Continue");
        btnContinue.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                MusicDataConfMenu.this.setDatasetAndContinue();
            }
        });
        this.error_label = new JLabel("");
        GridBagConstraints gbc_error_label = new GridBagConstraints();
        gbc_error_label.gridwidth = 8;
        gbc_error_label.insets = new Insets(20, 0, 5, 0);
        gbc_error_label.gridx = 1;
        gbc_error_label.gridy = 3;
        this.contentPane.add((Component)this.error_label, gbc_error_label);
        GridBagConstraints gbc_btnContinue = new GridBagConstraints();
        gbc_btnContinue.anchor = 13;
        gbc_btnContinue.insets = new Insets(20, 20, 20, 20);
        gbc_btnContinue.gridx = 8;
        gbc_btnContinue.gridy = 4;
        this.contentPane.add((Component)btnContinue, gbc_btnContinue);
    }

    private void setDatasetAndContinue() {
        if (this.textField.getText().equals("")) {
            this.error_label.setText("Please fill the votes percentage field.");
        } else {
            boolean check;
            double votes_perc = Double.parseDouble(this.textField.getText());
            boolean bl = check = votes_perc > 0.0 && votes_perc <= 1.0;
            if (check) {
                BaseDataset music = MusicData.createDataset(votes_perc);
                if (this.test.equals(BaconMenu.TEST_OPTIONS[BaconMenu.COMM_DET_INDEX])) {
                    CommunityDetectionMenu comm_det_menu = new CommunityDetectionMenu(music);
                    comm_det_menu.setVisible(true);
                    this.dispose();
                } else if (this.test.equals(BaconMenu.TEST_OPTIONS[BaconMenu.TUNING_INDEX])) {
                    TuningMenu tuning_menu = new TuningMenu(music);
                    tuning_menu.setVisible(true);
                    this.dispose();
                }
            } else {
                this.error_label.setText("Value out of range [0,1]. Please provide a new value.");
            }
        }
    }

}

