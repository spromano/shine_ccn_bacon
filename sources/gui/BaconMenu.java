package bacon.gui;

import bacon.gui.Artificial1ConfMenu;
import bacon.gui.Artificial2ConfMenu;
import bacon.gui.MovielensSettings;
import bacon.gui.MusicDataConfMenu;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class BaconMenu
extends JFrame {
    public static int COMM_DET_INDEX = 0;
    public static int TUNING_INDEX = 1;
    public static int MUSIC_INDEX = 0;
    public static int ARTIFICIAL1_INDEX = 1;
    public static int ARTIFICIAL2_INDEX = 2;
    public static int MOVIELENS_INDEX = 3;
    public static String[] TEST_OPTIONS = new String[]{"basic cycle", "tuning and performance test"};
    public static String[] DATASET_OPTIONS = new String[]{"music", "artificial_1", "artificial_2", "movielens"};
    private JPanel contentPane;
    private JComboBox test_type_comboBox;
    private JComboBox dataset_type_comboBox;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable(){

            @Override
            public void run() {
                try {
                    BaconMenu frame = new BaconMenu();
                    frame.setVisible(true);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public BaconMenu() {
        this.setTitle("BACON - Menu");
        this.setDefaultCloseOperation(3);
        this.setBounds(100, 100, 495, 280);
        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setContentPane(this.contentPane);
        GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[9];
        gbl_contentPane.rowHeights = new int[13];
        gbl_contentPane.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, Double.MIN_VALUE};
        gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        this.contentPane.setLayout(gbl_contentPane);
        JLabel lblSelectTestType = new JLabel("Select test type:");
        GridBagConstraints gbc_lblSelectTestType = new GridBagConstraints();
        gbc_lblSelectTestType.anchor = 17;
        gbc_lblSelectTestType.insets = new Insets(20, 20, 5, 5);
        gbc_lblSelectTestType.gridx = 2;
        gbc_lblSelectTestType.gridy = 3;
        this.contentPane.add((Component)lblSelectTestType, gbc_lblSelectTestType);
        JLabel lblSelectDataType = new JLabel("Select data type:");
        GridBagConstraints gbc_lblSelectDataType = new GridBagConstraints();
        gbc_lblSelectDataType.anchor = 17;
        gbc_lblSelectDataType.insets = new Insets(20, 5, 5, 20);
        gbc_lblSelectDataType.gridx = 3;
        gbc_lblSelectDataType.gridy = 3;
        this.contentPane.add((Component)lblSelectDataType, gbc_lblSelectDataType);
        this.test_type_comboBox = new JComboBox<String>(TEST_OPTIONS);
        GridBagConstraints gbc_test_type_comboBox = new GridBagConstraints();
        gbc_test_type_comboBox.insets = new Insets(5, 20, 5, 5);
        gbc_test_type_comboBox.gridx = 2;
        gbc_test_type_comboBox.gridy = 5;
        this.contentPane.add((Component)this.test_type_comboBox, gbc_test_type_comboBox);
        this.dataset_type_comboBox = new JComboBox<String>(DATASET_OPTIONS);
        GridBagConstraints gbc_dataset_type_comboBox = new GridBagConstraints();
        gbc_dataset_type_comboBox.ipadx = 70;
        gbc_dataset_type_comboBox.insets = new Insets(5, 5, 5, 20);
        gbc_dataset_type_comboBox.fill = 2;
        gbc_dataset_type_comboBox.gridx = 3;
        gbc_dataset_type_comboBox.gridy = 5;
        this.contentPane.add((Component)this.dataset_type_comboBox, gbc_dataset_type_comboBox);
        JButton btnSubmit = new JButton("Submit");
        btnSubmit.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                BaconMenu.this.create_configuration_window();
            }
        });
        btnSubmit.setVerticalAlignment(3);
        GridBagConstraints gbc_btnSubmit = new GridBagConstraints();
        gbc_btnSubmit.insets = new Insets(80, 5, 20, 20);
        gbc_btnSubmit.gridx = 3;
        gbc_btnSubmit.gridy = 8;
        this.contentPane.add((Component)btnSubmit, gbc_btnSubmit);
    }

    private void create_configuration_window() {
        String test = TEST_OPTIONS[this.test_type_comboBox.getSelectedIndex()];
        String dataset = DATASET_OPTIONS[this.dataset_type_comboBox.getSelectedIndex()];
        if (dataset.equals(DATASET_OPTIONS[ARTIFICIAL1_INDEX])) {
            Artificial1ConfMenu art1_data_menu = new Artificial1ConfMenu(test);
            art1_data_menu.setVisible(true);
            this.dispose();
        } else if (dataset.equals(DATASET_OPTIONS[ARTIFICIAL2_INDEX])) {
            Artificial2ConfMenu art2_data_menu = new Artificial2ConfMenu(test);
            art2_data_menu.setVisible(true);
            this.dispose();
        } else if (dataset.equals(DATASET_OPTIONS[MUSIC_INDEX])) {
            MusicDataConfMenu music_conf = new MusicDataConfMenu(test);
            music_conf.setVisible(true);
            this.dispose();
        } else {
            MovielensSettings movielens_settings = new MovielensSettings(test);
            movielens_settings.setVisible(true);
            this.dispose();
        }
    }

}

