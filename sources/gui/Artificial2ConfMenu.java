package bacon.gui;

import bacon.data.ArtificialDataLoader_Jaccard;
import bacon.gui.BaconMenu;
import bacon.gui.CommunityDetectionMenu;
import bacon.gui.TuningMenu;
import iweb2.ch3.collaborative.model.Dataset;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class Artificial2ConfMenu
extends JFrame {
    private JPanel contentPane;
    private JTextField num_users_textField;
    private JTextField num_items_textField;
    private JTextField votes_perc_textField;
    private JLabel error_textField;
    private String test;
    private JLabel lblDatasetDescription;
    private JTextArea textArea;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable(){

            @Override
            public void run() {
                try {
                    String test = BaconMenu.TEST_OPTIONS[BaconMenu.COMM_DET_INDEX];
                    Artificial2ConfMenu frame = new Artificial2ConfMenu(test);
                    frame.setVisible(true);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public Artificial2ConfMenu(String test) {
        this.test = test;
        this.setTitle("Artificial_2 Dataset Settings");
        this.setDefaultCloseOperation(3);
        this.setBounds(100, 100, 646, 440);
        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setContentPane(this.contentPane);
        GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[3];
        gbl_contentPane.rowHeights = new int[8];
        gbl_contentPane.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
        gbl_contentPane.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        this.contentPane.setLayout(gbl_contentPane);
        this.lblDatasetDescription = new JLabel("Dataset description:");
        GridBagConstraints gbc_lblDatasetDescription = new GridBagConstraints();
        gbc_lblDatasetDescription.anchor = 17;
        gbc_lblDatasetDescription.insets = new Insets(20, 20, 5, 5);
        gbc_lblDatasetDescription.gridx = 0;
        gbc_lblDatasetDescription.gridy = 0;
        this.contentPane.add((Component)this.lblDatasetDescription, gbc_lblDatasetDescription);
        String description = "This dataset contains 4 classes:\n- (A-B) users voting for (A-B) items with high rating 5\n- (C-D) users voting for (C-D) items with rating 4\n- (E-H) users voting for (E-H) items with rating 1\n- (I-Z) users voting for (I-Z) items with ratings (2-3)\nYou can configure:\n- the users scale factor U (you will have U*26 users)\n- the items scale factor I (you will have I*26 users)\n- the percentage of extra items each user can vote for.";
        this.textArea = new JTextArea(description);
        this.textArea.setEditable(false);
        GridBagConstraints gbc_textArea = new GridBagConstraints();
        gbc_textArea.gridwidth = 2;
        gbc_textArea.insets = new Insets(5, 20, 5, 20);
        gbc_textArea.fill = 1;
        gbc_textArea.gridx = 0;
        gbc_textArea.gridy = 1;
        this.contentPane.add((Component)this.textArea, gbc_textArea);
        JLabel lblNumUsers = new JLabel("users scale factor");
        GridBagConstraints gbc_lblNumUsers = new GridBagConstraints();
        gbc_lblNumUsers.anchor = 13;
        gbc_lblNumUsers.insets = new Insets(20, 5, 5, 5);
        gbc_lblNumUsers.gridx = 0;
        gbc_lblNumUsers.gridy = 2;
        this.contentPane.add((Component)lblNumUsers, gbc_lblNumUsers);
        this.num_users_textField = new JTextField();
        GridBagConstraints gbc_num_users_textField = new GridBagConstraints();
        gbc_num_users_textField.insets = new Insets(20, 5, 5, 20);
        gbc_num_users_textField.fill = 2;
        gbc_num_users_textField.gridx = 1;
        gbc_num_users_textField.gridy = 2;
        this.contentPane.add((Component)this.num_users_textField, gbc_num_users_textField);
        this.num_users_textField.setColumns(10);
        JLabel lblNumItems = new JLabel("items scale factor");
        GridBagConstraints gbc_lblNumItems = new GridBagConstraints();
        gbc_lblNumItems.anchor = 13;
        gbc_lblNumItems.insets = new Insets(5, 5, 5, 5);
        gbc_lblNumItems.gridx = 0;
        gbc_lblNumItems.gridy = 3;
        this.contentPane.add((Component)lblNumItems, gbc_lblNumItems);
        this.num_items_textField = new JTextField();
        GridBagConstraints gbc_num_items_textField = new GridBagConstraints();
        gbc_num_items_textField.insets = new Insets(5, 5, 5, 20);
        gbc_num_items_textField.fill = 2;
        gbc_num_items_textField.gridx = 1;
        gbc_num_items_textField.gridy = 3;
        this.contentPane.add((Component)this.num_items_textField, gbc_num_items_textField);
        this.num_items_textField.setColumns(10);
        JLabel lblOfExtra = new JLabel("% of extra voted item");
        GridBagConstraints gbc_lblOfExtra = new GridBagConstraints();
        gbc_lblOfExtra.anchor = 13;
        gbc_lblOfExtra.insets = new Insets(5, 5, 5, 5);
        gbc_lblOfExtra.gridx = 0;
        gbc_lblOfExtra.gridy = 4;
        this.contentPane.add((Component)lblOfExtra, gbc_lblOfExtra);
        this.votes_perc_textField = new JTextField();
        GridBagConstraints gbc_vote_perc_textField = new GridBagConstraints();
        gbc_vote_perc_textField.insets = new Insets(5, 5, 5, 20);
        gbc_vote_perc_textField.fill = 2;
        gbc_vote_perc_textField.gridx = 1;
        gbc_vote_perc_textField.gridy = 4;
        this.contentPane.add((Component)this.votes_perc_textField, gbc_vote_perc_textField);
        this.votes_perc_textField.setColumns(10);
        this.error_textField = new JLabel();
        GridBagConstraints gbc_error_textField = new GridBagConstraints();
        gbc_error_textField.anchor = 13;
        gbc_error_textField.insets = new Insets(5, 5, 5, 20);
        gbc_error_textField.gridwidth = 2;
        gbc_error_textField.gridx = 0;
        gbc_error_textField.gridy = 5;
        this.contentPane.add((Component)this.error_textField, gbc_error_textField);
        JButton btnContinue = new JButton("Continue");
        btnContinue.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                Artificial2ConfMenu.this.continue_action();
            }
        });
        GridBagConstraints gbc_btnContinue = new GridBagConstraints();
        gbc_btnContinue.insets = new Insets(5, 5, 20, 20);
        gbc_btnContinue.anchor = 13;
        gbc_btnContinue.gridx = 1;
        gbc_btnContinue.gridy = 6;
        this.contentPane.add((Component)btnContinue, gbc_btnContinue);
    }

    private void continue_action() {
        if (this.num_items_textField.getText().equals("") || this.num_users_textField.getText().equals("") || this.votes_perc_textField.getText().equals("")) {
            this.error_textField.setText("Please fill all the fields.");
        } else {
            boolean check;
            int users = Integer.parseInt(this.num_users_textField.getText());
            int items = Integer.parseInt(this.num_items_textField.getText());
            double perc_votes = Double.parseDouble(this.votes_perc_textField.getText());
            boolean bl = check = users > 0 && users < 500 && items > 0 && items < 500 && perc_votes >= 0.0 && perc_votes < 1.0;
            if (check) {
                ArtificialDataLoader_Jaccard loader = new ArtificialDataLoader_Jaccard(users, items, perc_votes);
                Dataset dataset = loader.loadDataset();
                if (this.test.equals(BaconMenu.TEST_OPTIONS[0])) {
                    CommunityDetectionMenu comm_det_menu = new CommunityDetectionMenu(dataset);
                    comm_det_menu.setVisible(true);
                    this.dispose();
                } else if (this.test.equals(BaconMenu.TEST_OPTIONS[1])) {
                    TuningMenu tuning_menu = new TuningMenu(dataset);
                    tuning_menu.setVisible(true);
                    this.dispose();
                }
            } else {
                this.error_textField.setText("Unacceptable parameters. Please provide new values.");
            }
        }
    }

}

