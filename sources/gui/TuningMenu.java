package bacon.gui;

import bacon.data.ArtificialDataLoader;
import bacon.gui.TuningResults;
import iweb2.ch3.collaborative.model.Dataset;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class TuningMenu
extends JFrame {
    private JPanel contentPane;
    private JTextField min_eps_textField;
    private JLabel lblMaxEpsilon;
    private JTextField max_eps_textField;
    private JLabel lblEpsilonStep;
    private JTextField eps_step_textField;
    private JLabel lblMinMinpoints;
    private JTextField min_mipoints_textField;
    private JLabel lblMaxMinpoints;
    private JTextField max_minpoints_textField;
    private JLabel lblMinpointsStep;
    private JTextField minpoints_step_textField;
    private JCheckBox chckbxComputeClusterProperties;
    private JButton btnStartTuning;
    private Dataset dataset;
    private JLabel error_label;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable(){

            @Override
            public void run() {
                try {
                    ArtificialDataLoader dl = new ArtificialDataLoader(1, 1, 2);
                    Dataset ds = dl.loadDataset();
                    TuningMenu frame = new TuningMenu(ds);
                    frame.setVisible(true);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public TuningMenu(Dataset ds) {
        this.dataset = ds;
        this.setTitle("Tuning settings");
        this.setDefaultCloseOperation(3);
        this.setBounds(100, 100, 460, 362);
        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setContentPane(this.contentPane);
        GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[3];
        gbl_contentPane.rowHeights = new int[15];
        gbl_contentPane.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        this.contentPane.setLayout(gbl_contentPane);
        JLabel lblMinEpsilon = new JLabel("min epsilon");
        GridBagConstraints gbc_lblMinEpsilon = new GridBagConstraints();
        gbc_lblMinEpsilon.insets = new Insets(20, 5, 5, 5);
        gbc_lblMinEpsilon.anchor = 13;
        gbc_lblMinEpsilon.gridx = 0;
        gbc_lblMinEpsilon.gridy = 2;
        this.contentPane.add((Component)lblMinEpsilon, gbc_lblMinEpsilon);
        this.min_eps_textField = new JTextField("0.50");
        GridBagConstraints gbc_min_eps_textField = new GridBagConstraints();
        gbc_min_eps_textField.insets = new Insets(20, 5, 5, 20);
        gbc_min_eps_textField.fill = 2;
        gbc_min_eps_textField.gridx = 1;
        gbc_min_eps_textField.gridy = 2;
        this.contentPane.add((Component)this.min_eps_textField, gbc_min_eps_textField);
        this.min_eps_textField.setColumns(10);
        this.lblMaxEpsilon = new JLabel("max epsilon");
        GridBagConstraints gbc_lblMaxEpsilon = new GridBagConstraints();
        gbc_lblMaxEpsilon.anchor = 13;
        gbc_lblMaxEpsilon.insets = new Insets(5, 5, 5, 5);
        gbc_lblMaxEpsilon.gridx = 0;
        gbc_lblMaxEpsilon.gridy = 3;
        this.contentPane.add((Component)this.lblMaxEpsilon, gbc_lblMaxEpsilon);
        this.max_eps_textField = new JTextField("0.95");
        GridBagConstraints gbc_max_eps_textField = new GridBagConstraints();
        gbc_max_eps_textField.insets = new Insets(5, 5, 5, 20);
        gbc_max_eps_textField.fill = 2;
        gbc_max_eps_textField.gridx = 1;
        gbc_max_eps_textField.gridy = 3;
        this.contentPane.add((Component)this.max_eps_textField, gbc_max_eps_textField);
        this.max_eps_textField.setColumns(10);
        this.lblEpsilonStep = new JLabel("epsilon step");
        GridBagConstraints gbc_lblEpsilonStep = new GridBagConstraints();
        gbc_lblEpsilonStep.anchor = 13;
        gbc_lblEpsilonStep.insets = new Insets(5, 5, 5, 5);
        gbc_lblEpsilonStep.gridx = 0;
        gbc_lblEpsilonStep.gridy = 4;
        this.contentPane.add((Component)this.lblEpsilonStep, gbc_lblEpsilonStep);
        this.eps_step_textField = new JTextField("0.10");
        GridBagConstraints gbc_eps_step_textField = new GridBagConstraints();
        gbc_eps_step_textField.insets = new Insets(5, 5, 5, 20);
        gbc_eps_step_textField.fill = 2;
        gbc_eps_step_textField.gridx = 1;
        gbc_eps_step_textField.gridy = 4;
        this.contentPane.add((Component)this.eps_step_textField, gbc_eps_step_textField);
        this.eps_step_textField.setColumns(10);
        this.lblMinMinpoints = new JLabel("min minpoints");
        GridBagConstraints gbc_lblMinMinpoints = new GridBagConstraints();
        gbc_lblMinMinpoints.anchor = 13;
        gbc_lblMinMinpoints.insets = new Insets(5, 5, 5, 5);
        gbc_lblMinMinpoints.gridx = 0;
        gbc_lblMinMinpoints.gridy = 5;
        this.contentPane.add((Component)this.lblMinMinpoints, gbc_lblMinMinpoints);
        this.min_mipoints_textField = new JTextField("2");
        GridBagConstraints gbc_min_mipoints_textField = new GridBagConstraints();
        gbc_min_mipoints_textField.insets = new Insets(5, 5, 5, 20);
        gbc_min_mipoints_textField.fill = 2;
        gbc_min_mipoints_textField.gridx = 1;
        gbc_min_mipoints_textField.gridy = 5;
        this.contentPane.add((Component)this.min_mipoints_textField, gbc_min_mipoints_textField);
        this.min_mipoints_textField.setColumns(10);
        this.lblMaxMinpoints = new JLabel("max minpoints");
        GridBagConstraints gbc_lblMaxMinpoints = new GridBagConstraints();
        gbc_lblMaxMinpoints.anchor = 13;
        gbc_lblMaxMinpoints.insets = new Insets(5, 5, 5, 5);
        gbc_lblMaxMinpoints.gridx = 0;
        gbc_lblMaxMinpoints.gridy = 6;
        this.contentPane.add((Component)this.lblMaxMinpoints, gbc_lblMaxMinpoints);
        this.max_minpoints_textField = new JTextField("7");
        GridBagConstraints gbc_max_minpoints_textField = new GridBagConstraints();
        gbc_max_minpoints_textField.insets = new Insets(5, 5, 5, 20);
        gbc_max_minpoints_textField.fill = 2;
        gbc_max_minpoints_textField.gridx = 1;
        gbc_max_minpoints_textField.gridy = 6;
        this.contentPane.add((Component)this.max_minpoints_textField, gbc_max_minpoints_textField);
        this.max_minpoints_textField.setColumns(10);
        this.lblMinpointsStep = new JLabel("minpoints step");
        GridBagConstraints gbc_lblMinpointsStep = new GridBagConstraints();
        gbc_lblMinpointsStep.anchor = 13;
        gbc_lblMinpointsStep.insets = new Insets(5, 5, 5, 5);
        gbc_lblMinpointsStep.gridx = 0;
        gbc_lblMinpointsStep.gridy = 7;
        this.contentPane.add((Component)this.lblMinpointsStep, gbc_lblMinpointsStep);
        this.minpoints_step_textField = new JTextField("1");
        GridBagConstraints gbc_minpoints_step_textField = new GridBagConstraints();
        gbc_minpoints_step_textField.insets = new Insets(5, 5, 5, 20);
        gbc_minpoints_step_textField.fill = 2;
        gbc_minpoints_step_textField.gridx = 1;
        gbc_minpoints_step_textField.gridy = 7;
        this.contentPane.add((Component)this.minpoints_step_textField, gbc_minpoints_step_textField);
        this.minpoints_step_textField.setColumns(10);
        this.chckbxComputeClusterProperties = new JCheckBox("Compute cluster properties");
        GridBagConstraints gbc_chckbxComputeClusterProperties = new GridBagConstraints();
        gbc_chckbxComputeClusterProperties.anchor = 17;
        gbc_chckbxComputeClusterProperties.insets = new Insets(5, 5, 5, 0);
        gbc_chckbxComputeClusterProperties.gridx = 1;
        gbc_chckbxComputeClusterProperties.gridy = 8;
        this.contentPane.add((Component)this.chckbxComputeClusterProperties, gbc_chckbxComputeClusterProperties);
        this.btnStartTuning = new JButton("Submit");
        this.btnStartTuning.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                TuningMenu.this.start_tuning();
            }
        });
        this.error_label = new JLabel("");
        GridBagConstraints gbc_error_label = new GridBagConstraints();
        gbc_error_label.anchor = 13;
        gbc_error_label.insets = new Insets(5, 5, 5, 20);
        gbc_error_label.gridx = 1;
        gbc_error_label.gridy = 9;
        this.contentPane.add((Component)this.error_label, gbc_error_label);
        GridBagConstraints gbc_btnStartTuning = new GridBagConstraints();
        gbc_btnStartTuning.anchor = 13;
        gbc_btnStartTuning.insets = new Insets(5, 5, 20, 20);
        gbc_btnStartTuning.gridx = 1;
        gbc_btnStartTuning.gridy = 10;
        this.contentPane.add((Component)this.btnStartTuning, gbc_btnStartTuning);
    }

    private void start_tuning() {
        if (this.min_eps_textField.getText().equals("") || this.max_eps_textField.getText().equals("") || this.eps_step_textField.getText().equals("") || this.min_mipoints_textField.getText().equals("") || this.max_minpoints_textField.getText().equals("") || this.minpoints_step_textField.getText().equals("")) {
            this.error_label.setText("Please fill all the fields.");
        } else {
            boolean check;
            double min_eps = Double.parseDouble(this.min_eps_textField.getText());
            double max_eps = Double.parseDouble(this.max_eps_textField.getText());
            double eps_step = Double.parseDouble(this.eps_step_textField.getText());
            int min_points = Integer.parseInt(this.min_mipoints_textField.getText());
            int max_points = Integer.parseInt(this.max_minpoints_textField.getText());
            int minpoints_step = Integer.parseInt(this.minpoints_step_textField.getText());
            boolean clus_props = this.chckbxComputeClusterProperties.isSelected();
            boolean bl = check = min_eps > 0.0 && min_eps < 1.0 && max_eps > 0.0 && max_eps < 1.0 && min_eps < max_eps && eps_step > 0.0 && eps_step < 1.0 && min_points >= 2 && min_points < 10 && max_points > 2 && max_points < 10 && minpoints_step >= 1 && minpoints_step < 10;
            if (check) {
                TuningResults tr = new TuningResults(this.dataset, min_eps, max_eps, eps_step, min_points, max_points, minpoints_step, clus_props);
                tr.setVisible(true);
                this.dispose();
            } else {
                this.min_eps_textField.setText("");
                this.max_eps_textField.setText("");
                this.eps_step_textField.setText("");
                this.min_mipoints_textField.setText("");
                this.max_minpoints_textField.setText("");
                this.minpoints_step_textField.setText("");
                this.error_label.setText("Unacceptable parameters.");
            }
        }
    }

}

