package bacon.gui;

import bacon.analyzer.BaconAnalyzerImpl;
import bacon.analyzer.Validator;
import bacon.clustering.ClusteringEvaluation;
import bacon.clustering.DBSCANAlgorithm;
import bacon.data.ArtificialDataLoader;
import bacon.gui.BaconMenu;
import bacon.utils.Chronometer;
import iweb2.ch3.collaborative.model.Dataset;
import iweb2.ch3.collaborative.model.Item;
import iweb2.ch3.collaborative.model.User;
import iweb2.ch4.model.Cluster;
import iweb2.ch4.model.DataPoint;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class ResultsAndContentEvaluationWindow
extends JFrame {
    private JPanel contentPane;
    private JTextField content_id_textField;
    private JTextArea utility_vector_textField;
    private JTextArea utility_textField;
    private JTextArea detection_summary_textArea;
    private JTextArea detection_details_textArea;
    private BaconAnalyzerImpl analyzer;
    private Dataset dataset;
    private double epsilon;
    private int minpoints;
    private boolean clus_props;
    private boolean validation;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable(){

            @Override
            public void run() {
                try {
                    ArtificialDataLoader loader = new ArtificialDataLoader(1, 1, 2, 1.0);
                    Dataset dataset = loader.loadDataset();
                    boolean one = false;
                    boolean two = false;
                    int m = 2;
                    double e = 0.8;
                    ResultsAndContentEvaluationWindow frame = new ResultsAndContentEvaluationWindow(dataset, e, m, one, two);
                    frame.setVisible(true);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public ResultsAndContentEvaluationWindow(Dataset ds, double eps, int minp, boolean cluster_props, boolean valid) {
        this.dataset = ds;
        this.epsilon = eps;
        this.minpoints = minp;
        this.clus_props = cluster_props;
        this.validation = valid;
        this.setTitle("Analysis Panel");
        this.setDefaultCloseOperation(3);
        this.setBounds(100, 100, 770, 531);
        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setContentPane(this.contentPane);
        GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[8];
        gbl_contentPane.rowHeights = new int[9];
        gbl_contentPane.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        this.contentPane.setLayout(gbl_contentPane);
        JButton button = new JButton("Start community detection");
        button.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                ResultsAndContentEvaluationWindow.this.analyze(ResultsAndContentEvaluationWindow.this.dataset, ResultsAndContentEvaluationWindow.this.epsilon, ResultsAndContentEvaluationWindow.this.minpoints, ResultsAndContentEvaluationWindow.this.clus_props, ResultsAndContentEvaluationWindow.this.validation);
            }
        });
        GridBagConstraints gbc_button = new GridBagConstraints();
        gbc_button.gridwidth = 6;
        gbc_button.insets = new Insets(20, 5, 20, 5);
        gbc_button.gridx = 0;
        gbc_button.gridy = 0;
        this.contentPane.add((Component)button, gbc_button);
        JLabel lblCommunityDetectionSummary = new JLabel("Community Detection Summary");
        GridBagConstraints gbc_lblCommunityDetectionSummary = new GridBagConstraints();
        gbc_lblCommunityDetectionSummary.insets = new Insets(0, 0, 5, 5);
        gbc_lblCommunityDetectionSummary.gridx = 0;
        gbc_lblCommunityDetectionSummary.gridy = 1;
        this.contentPane.add((Component)lblCommunityDetectionSummary, gbc_lblCommunityDetectionSummary);
        JLabel lblCommunityDetectionDetails = new JLabel("Community Detection Details");
        GridBagConstraints gbc_lblCommunityDetectionDetails = new GridBagConstraints();
        gbc_lblCommunityDetectionDetails.gridwidth = 2;
        gbc_lblCommunityDetectionDetails.insets = new Insets(0, 0, 5, 5);
        gbc_lblCommunityDetectionDetails.gridx = 4;
        gbc_lblCommunityDetectionDetails.gridy = 1;
        this.contentPane.add((Component)lblCommunityDetectionDetails, gbc_lblCommunityDetectionDetails);
        this.detection_summary_textArea = new JTextArea();
        this.detection_summary_textArea.setEditable(false);
        JScrollPane scrollBar = new JScrollPane(this.detection_summary_textArea);
        GridBagConstraints gbc_detection_summary_textArea = new GridBagConstraints();
        gbc_detection_summary_textArea.gridwidth = 2;
        gbc_detection_summary_textArea.insets = new Insets(0, 0, 5, 5);
        gbc_detection_summary_textArea.fill = 1;
        gbc_detection_summary_textArea.gridx = 0;
        gbc_detection_summary_textArea.gridy = 2;
        this.contentPane.add((Component)scrollBar, gbc_detection_summary_textArea);
        GridBagConstraints gbc_detection_details_textArea = new GridBagConstraints();
        gbc_detection_details_textArea.gridwidth = 2;
        gbc_detection_details_textArea.insets = new Insets(0, 0, 5, 5);
        gbc_detection_details_textArea.fill = 1;
        gbc_detection_details_textArea.gridx = 4;
        gbc_detection_details_textArea.gridy = 2;
        this.detection_details_textArea = new JTextArea();
        this.detection_details_textArea.setColumns(2);
        this.detection_details_textArea.setEditable(false);
        JScrollPane scrollBar_1 = new JScrollPane(this.detection_details_textArea);
        this.contentPane.add((Component)scrollBar_1, gbc_detection_details_textArea);
        JLabel lblContentEvaluation = new JLabel("Content Evaluation");
        GridBagConstraints gbc_lblContentEvaluation = new GridBagConstraints();
        gbc_lblContentEvaluation.insets = new Insets(0, 0, 5, 5);
        gbc_lblContentEvaluation.gridx = 0;
        gbc_lblContentEvaluation.gridy = 3;
        this.contentPane.add((Component)lblContentEvaluation, gbc_lblContentEvaluation);
        JLabel lblContentId = new JLabel("Content ID:");
        GridBagConstraints gbc_lblContentId = new GridBagConstraints();
        gbc_lblContentId.anchor = 13;
        gbc_lblContentId.insets = new Insets(0, 0, 5, 5);
        gbc_lblContentId.gridx = 0;
        gbc_lblContentId.gridy = 4;
        this.contentPane.add((Component)lblContentId, gbc_lblContentId);
        this.content_id_textField = new JTextField();
        GridBagConstraints gbc_content_id_textField = new GridBagConstraints();
        gbc_content_id_textField.insets = new Insets(0, 0, 5, 5);
        gbc_content_id_textField.fill = 2;
        gbc_content_id_textField.gridx = 4;
        gbc_content_id_textField.gridy = 4;
        this.contentPane.add((Component)this.content_id_textField, gbc_content_id_textField);
        this.content_id_textField.setColumns(10);
        JButton btnComputeUtilities = new JButton("Compute utilities...");
        btnComputeUtilities.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                ResultsAndContentEvaluationWindow.this.compute_utilities(ResultsAndContentEvaluationWindow.this.dataset);
            }
        });
        GridBagConstraints gbc_btnComputeUtilities = new GridBagConstraints();
        gbc_btnComputeUtilities.insets = new Insets(0, 0, 5, 5);
        gbc_btnComputeUtilities.gridx = 5;
        gbc_btnComputeUtilities.gridy = 4;
        this.contentPane.add((Component)btnComputeUtilities, gbc_btnComputeUtilities);
        JLabel lblContentUtility = new JLabel("Utility vector = ");
        GridBagConstraints gbc_lblContentUtility = new GridBagConstraints();
        gbc_lblContentUtility.anchor = 13;
        gbc_lblContentUtility.insets = new Insets(0, 0, 5, 5);
        gbc_lblContentUtility.gridx = 0;
        gbc_lblContentUtility.gridy = 5;
        this.contentPane.add((Component)lblContentUtility, gbc_lblContentUtility);
        this.utility_vector_textField = new JTextArea();
        GridBagConstraints gbc_utility_vector_textField = new GridBagConstraints();
        gbc_utility_vector_textField.insets = new Insets(0, 0, 5, 5);
        gbc_utility_vector_textField.fill = 2;
        gbc_utility_vector_textField.gridx = 4;
        gbc_utility_vector_textField.gridy = 5;
        this.contentPane.add((Component)this.utility_vector_textField, gbc_utility_vector_textField);
        this.utility_vector_textField.setColumns(10);
        JLabel lblContentUtility_1 = new JLabel("Content Utility = ");
        GridBagConstraints gbc_lblContentUtility_1 = new GridBagConstraints();
        gbc_lblContentUtility_1.anchor = 13;
        gbc_lblContentUtility_1.insets = new Insets(0, 0, 5, 5);
        gbc_lblContentUtility_1.gridx = 0;
        gbc_lblContentUtility_1.gridy = 6;
        this.contentPane.add((Component)lblContentUtility_1, gbc_lblContentUtility_1);
        this.utility_textField = new JTextArea();
        GridBagConstraints gbc_utility_textField = new GridBagConstraints();
        gbc_utility_textField.insets = new Insets(0, 0, 5, 5);
        gbc_utility_textField.fill = 2;
        gbc_utility_textField.gridx = 4;
        gbc_utility_textField.gridy = 6;
        this.contentPane.add((Component)this.utility_textField, gbc_utility_textField);
        this.utility_textField.setColumns(10);
        JButton btnNewTest = new JButton("New Test");
        btnNewTest.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                ResultsAndContentEvaluationWindow.this.new_test();
            }
        });
        GridBagConstraints gbc_btnNewTest = new GridBagConstraints();
        gbc_btnNewTest.anchor = 13;
        gbc_btnNewTest.insets = new Insets(5, 5, 20, 20);
        gbc_btnNewTest.gridx = 5;
        gbc_btnNewTest.gridy = 7;
        this.contentPane.add((Component)btnNewTest, gbc_btnNewTest);
    }

    private void new_test() {
        BaconMenu menu = new BaconMenu();
        menu.setVisible(true);
        this.dispose();
    }

    private void compute_utilities(Dataset ds) {
        if (this.content_id_textField.getText().equals("")) {
            this.content_id_textField.setText("---insert a content id here---");
        } else {
            int item_id = Integer.parseInt(this.content_id_textField.getText());
            if (ds.getItem(item_id) == null) {
                this.utility_vector_textField.setText("Content not found");
                this.utility_textField.setText("Content not found");
            } else {
                Item item = ds.getItem(item_id);
                Map<Integer, Double> vector = this.analyzer.getContentEvaluationVector(ds.getItem(item_id));
                this.content_id_textField.setText(String.valueOf(item_id) + " --> " + item.getName());
                this.utility_vector_textField.setText("");
                for (Integer i : vector.keySet()) {
                    this.utility_vector_textField.append("Community " + i + ") " + vector.get(i) + "\n");
                }
                this.utility_textField.setText("");
                this.utility_textField.append(String.valueOf(this.analyzer.getContentUtility(item)));
            }
        }
    }

    private void analyze(Dataset dataset, double epsilon, int minpoints, boolean cluster_props, boolean validation) {
        this.detection_details_textArea.append("\nDataset properties:\n");
        this.detection_details_textArea.append("# users = " + dataset.getUserCount() + "\n");
        this.detection_details_textArea.append("# items = " + dataset.getItemCount() + "\n");
        this.detection_details_textArea.append("# ratings = " + dataset.getRatingsCount() + "\n");
        Chronometer timer = new Chronometer();
        timer.startChronometer();
        this.detection_details_textArea.append("\nBuilding distance matrix...\n");
        this.analyzer = new BaconAnalyzerImpl(dataset, epsilon, minpoints);
        timer.stopChronometer();
        this.detection_details_textArea.append("Distance matrix built in " + timer.getElapsedTime() + " s\n");
        timer.startChronometer();
        this.detection_details_textArea.append("\nRunning community detection...\n");
        this.analyzer.detectCommunities();
        this.analyzer.detectTypicalUsers();
        timer.stopChronometer();
        this.detection_details_textArea.append("Community detection completed in " + timer.getElapsedTime() + " s\n");
        this.detection_details_textArea.append("\nDBSCAN Clustering with NeighborThreshold=" + epsilon + " minPoints=" + minpoints + "\n");
        this.detection_details_textArea.append("Clusters:\n");
        String noiseElements = "no noise elements";
        for (Cluster c : this.analyzer.getCommunities()) {
            if (String.valueOf(DBSCANAlgorithm.CLUSTER_ID_NOISE).equals(c.getLabel())) {
                noiseElements = c.getElementsAsString();
                continue;
            }
            this.detection_details_textArea.append("_______________________________________________\n");
            this.detection_details_textArea.append(String.valueOf(c.getLabel()) + ": \n" + c.getElementsAsString() + "\n");
            this.detection_details_textArea.append("_______________________________________________\n\n");
        }
        this.detection_details_textArea.append("Noise Elements: " + noiseElements + "\n");
        this.detection_details_textArea.append("\n");
        this.detection_details_textArea.append("\n");
        for (Integer i : this.analyzer.getTypicalUsers().keySet()) {
            this.detection_details_textArea.append("Typical user of community " + i + ": " + "name = " + this.analyzer.getTypicalUsers().get(i).getName() + ", id = " + this.analyzer.getTypicalUsers().get(i).getId() + "\n");
        }
        this.detection_summary_textArea.append("\nDBSCAN Clustering Summary\n");
        this.detection_summary_textArea.append("Parameters:\n epsilon = " + epsilon + ",  minPoints=" + minpoints + "\n");
        this.detection_summary_textArea.append("# clusters = " + this.analyzer.getCommunities().size() + "\n");
        boolean noiseElements_ = false;
        int num_noise = 0;
        for (Cluster c : this.analyzer.getCommunities()) {
            if (String.valueOf(DBSCANAlgorithm.CLUSTER_ID_NOISE).equals(c.getLabel())) {
                noiseElements_ = true;
                num_noise = c.getElements().size();
                continue;
            }
            this.detection_summary_textArea.append("Cluster " + c.getLabel() + ", # points = " + c.getElements().size() + "\n");
        }
        if (noiseElements_) {
            this.detection_summary_textArea.append("Noise Cluster, # points = " + num_noise + "\n");
        }
        this.detection_summary_textArea.append("\n");
        if (cluster_props) {
            timer.startChronometer();
            ClusteringEvaluation evaluator = new ClusteringEvaluation(this.analyzer.getCommunities(), this.analyzer.getDistanceMatrix());
            this.detection_details_textArea.append("\n*** Cluster properties (" + this.analyzer.getCommunities().size() + " discovered clusters) ***" + "\n");
            for (Cluster c : this.analyzer.getCommunities()) {
                this.detection_details_textArea.append("--- CLUSTER " + c.getLabel() + "\n");
                int clusterSize = c.size();
                this.detection_details_textArea.append(" size: " + clusterSize + "\n");
                this.detection_details_textArea.append(" average internal distance: " + evaluator.getAverageInternalDistance(c) + "\n");
                this.detection_details_textArea.append(" max internal distance: " + evaluator.getMaxInternalDistance(c) + "\n");
                for (Cluster other_c : this.analyzer.getCommunities()) {
                    if (c.getLabel().equals(other_c.getLabel())) continue;
                    this.detection_details_textArea.append(" distance from cluster " + other_c.getLabel() + ": " + "\n");
                    this.detection_details_textArea.append("  single link = " + evaluator.getSingleLinkDistance(c, other_c) + "\n");
                    this.detection_details_textArea.append("  average link = " + evaluator.getAverageLinkDistance(c, other_c) + "\n");
                    this.detection_details_textArea.append("  complete link = " + evaluator.getCompleteLinkDistance(c, other_c) + "\n");
                }
            }
            this.detection_details_textArea.append("*** End ***\n");
            timer.stopChronometer();
            this.detection_details_textArea.append("Cluster properties computed in " + timer.getElapsedTime() + " s\n");
        }
        if (validation) {
            this.detection_details_textArea.append("\nStarting validation...\n");
            timer.startChronometer();
            Validator validator = new Validator(this.analyzer);
            if (validator.isThereNoise()) {
                this.detection_details_textArea.append("Noise: " + validator.getNoisePercentage() + "\n");
                this.detection_details_textArea.append("Accuracy (out of noise): " + validator.getPercentualAccuracyWithoutNoise() + "\n");
            } else {
                this.detection_details_textArea.append("No noise points.\n");
                this.detection_details_textArea.append("Accuracy: " + validator.getPercentualAccuracyWithoutNoise() + "\n");
            }
            timer.stopChronometer();
            this.detection_details_textArea.append("Validation completed in " + timer.getElapsedTime() + " s\n");
        }
    }

}

